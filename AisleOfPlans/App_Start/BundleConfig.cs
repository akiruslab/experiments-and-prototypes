﻿using System.Web;
using System.Web.Optimization;

namespace AisleOfPlans
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery.js",
                "~/Scripts/jquery.unobtrusive-ajax.min.js",
                 "~/Scripts/bootstrap.js",
                 "~/Scripts/jquery.validate.unobtrusive.js",
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/charts").Include(
                "~/Scripts/easypiechart.js", 
                "~/Scripts/chart.min.js",
                "~/Scripts/easypiechart-data.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/dashboard").Include(
                      "~/Scripts/easy-style.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/sb-admin.css"));
            bundles.Add(new StyleBundle("~/Content/css2").Include(
                     "~/Content/bootstrap.min.css",
                     "~/Content/font-awesome.min.css"));
        }
    }
}
