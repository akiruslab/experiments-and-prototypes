﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AisleOfPlans
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
            name: "Home",
            url: "Home",
            defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
           name: "About",
           url: "AboutUs",
           defaults: new { controller = "Home", action = "About", id = UrlParameter.Optional }
           );

            routes.MapRoute(
           name: "Contact",
           url: "ContactUs",
           defaults: new { controller = "Home", action = "Contact", id = UrlParameter.Optional }
           );

            ///*Admin substystem*///
            //////////AccountManagement/////////
            routes.MapRoute(
            name: "AccountAddition",
            url: "AddAccount/{id}",
            defaults: new { controller = "Account", action = "Register", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "AccountDisabling",
            url: "DiableAccount/{id}",
            defaults: new { controller = "Account", action = "Disable", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "AccountEnabling",
            url: "EnableAccount/{id}",
            defaults: new { controller = "Account", action = "Enable", id = UrlParameter.Optional }
            );

            //////////ContentManagement/////////
            routes.MapRoute(
              name: "ContentEdit",
              url: "EditContent/{id}",
              defaults: new { controller = "Content", action = "Edit", id = UrlParameter.Optional }
              );

            routes.MapRoute(
             name: "ContentAdd",
             url: "AddContent",
             defaults: new { controller = "Content", action = "Create", id = UrlParameter.Optional }
             );

            routes.MapRoute(
             name: "ContentDelete",
             url: "DeleteContent/{id}",
             defaults: new { controller = "Content", action = "Delete", id = UrlParameter.Optional }
             );

            routes.MapRoute(
             name: "ContentList",
             url: "ViewContent/{id}",
             defaults: new { controller = "Content", action = "ViewLIst", id = UrlParameter.Optional }
             );

             routes.MapRoute(
             name: "ContentCategories",
             url: "ViewCategories/{id}",
             defaults: new { controller = "Content", action = "Index", id = UrlParameter.Optional }
             );

            ///*Report substystem*///
            routes.MapRoute(
            name: "VendorReport",
            url: "ViewReports/{id}",
            defaults: new { controller = "Report", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "AdminRport",
            url: "ViewReports/{id}",
            defaults: new { controller = "Report", action = "Index", id = UrlParameter.Optional }
            );

            ///*Profile substystem*///
            routes.MapRoute(
            name: "Profile",
            url: "Profile",
            defaults: new { controller = "Profile", action = "Index", id = UrlParameter.Optional }
            );   
            /////////////////////Client////////////////
            routes.MapRoute(
            name: "ClientHome",
            url: "ClientHome",
            defaults: new { controller = "Profile", action = "LoadClientProfile", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "ClientCreate",
            url: "Setup-Profile",
            defaults: new { controller = "Profile", action = "CreateClientProfile", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "ClientEdit",
            url: "Edit_Profile",
            defaults: new { controller = "Profile", action = "EditClientProfile", id = UrlParameter.Optional }
            );
            ///////////////////Vendor//////////////////
            routes.MapRoute(
            name: "VendorHome",
            url: "VendorHome",
            defaults: new { controller = "Profile", action = "LoadVendorProfile", id = UrlParameter.Optional }
            );


            routes.MapRoute(
            name: "VendorCreate",
            url: "Setup_Profile",
            defaults: new { controller = "Profile", action = "CreateVendorProfile", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "VendorEdit",
            url: "Edit_VendorProfile",
            defaults: new { controller = "Profile", action = "EditVendorProfile", id = UrlParameter.Optional }
            );

            ///*Account substystem*///
            routes.MapRoute(
            name: "SignIn",
            url: "Login",
            defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "Register",
            url: "Register/{id}",
            defaults: new { controller = "Account", action = "Register", id = UrlParameter.Optional }
            );

          

            ///*Request substystem*///
            //////////Client View of Requests/////////
            routes.MapRoute(
            name: "ClientRequests",
            url: "ServiceRequests/{status}",
            defaults: new { controller = "Request", action = "Index", id = UrlParameter.Optional,
                status = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "ViewDetails",
            url: "Detials/{id}",
            defaults: new { controller = "Request", action = "Details", id = UrlParameter.Optional,
                status = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "CancelRequest",
            url: "CancelRequest/{id}",
            defaults: new { controller = "Request", action = "Cancel", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "CompleteRequest",
            url: "CompleteRequest/{id}",
            defaults: new { controller = "Request", action = "Complete", id = UrlParameter.Optional }
            );

            //////////Vendor View of Requests/////////
            routes.MapRoute(
            name: "DeclineRequest",
            url: "DeclineRequest/{id}",
            defaults: new { controller = "Request", action = "Decline", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "AcceptRequest",
            url: "AcceptRequest/{id}",
            defaults: new { controller = "Request", action = "Accept", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "VendorRequests",
            url: "ServiceRequestsForVendor/{status}",
            defaults: new { controller = "Request", action = "Index", id = UrlParameter.Optional
            , status = UrlParameter.Optional}
            );

            ///*Wedding substystem*///
            routes.MapRoute(
            name: "WeddingView",
            url: "WeddingDetails",
            defaults: new { controller = "Wedding", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
           name: "WeddingEdit",
           url: "EditWedding/{WeddingID}",
           defaults: new { controller = "Wedding", action = "Edit", WeddingID = UrlParameter.Optional }
           );

            routes.MapRoute(
            name: "WeddingCreate",
            url: "CreateWedding",
            defaults: new { controller = "Wedding", action = "Create", id = UrlParameter.Optional }
            );
            
            //////////Plan/////////
            routes.MapRoute(
            name: "PlanView",
            url: "CompletePlan/{id}",
            defaults: new { controller = "Plan", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "PlanEdit",
            url: "EditPlan/{id}",
            defaults: new { controller = "Plan", action = "Edit", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "PlanCancel",
            url: "Cancel/{id}",
            defaults: new { controller = "Plan", action = "Cancel", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "PlanStart",
            url: "ActivatePlan/{id}",
            defaults: new { controller = "Plan", action = "Activate", id = UrlParameter.Optional }
            );

            //////////Objective/////////
            routes.MapRoute(
            name: "ObjectiveView",
            url: "ViewObjectives/{id}",
            defaults: new { controller = "PlanObjective", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "ObjectiveDisable",
            url: "DisableObjective/{id}",
            defaults: new { controller = "PlanObjective", action = "Disable", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "ObjectiveEnable",
            url: "EnableObjective/{id}",
            defaults: new { controller = "PlanObjective", action = "Enable", id = UrlParameter.Optional }
            );


            ///*Vendor Management substystem*///
            //////////Item/////////
            routes.MapRoute(
            name: "ItemView",
            url: "ViewCatalogItems/{id}",
            defaults: new { controller = "Item", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "ObjectiveComplete",
            url: "Complete/{objective}",
            defaults: new { controller = "Item", action = "Browse", objective = UrlParameter.Optional
            }
            );

            routes.MapRoute(
            name: "ItemAdd",
            url: "AddToCatalogue",
            defaults: new { controller = "Item", action = "Create", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "ItemDelete",
            url: "DeleteFromCatalogue/{id}",
            defaults: new { controller = "Item", action = "Delete", id = UrlParameter.Optional }
            );

             routes.MapRoute(
             name: "ItemEdit",
             url: "EditItem/{id}",
             defaults: new { controller = "Item", action = "Edit", id = UrlParameter.Optional }
             );

            //////////Catalogue/////////
            routes.MapRoute(
             name: "CatalogueEdit",
             url: "EditCatalogue/{id}",
             defaults: new { controller = "Catalog", action = "Edit", id = UrlParameter.Optional }
             );

            routes.MapRoute(
             name: "CatalogueAdd",
             url: "AddCatalogue",
             defaults: new { controller = "Catalog", action = "Create", id = UrlParameter.Optional }
             );

            routes.MapRoute(
             name: "CatalogueDelete",
             url: "DeleteCatalogue/{id}",
             defaults: new { controller = "Catalog", action = "Delete", id = UrlParameter.Optional }
             );

            routes.MapRoute(
             name: "CatalogueList",
             url: "ViewCatalogues",
             defaults: new { controller = "Catalog", action = "Index", id = UrlParameter.Optional }
             );

            ////////////////////////////////////Default////////////////////////////////////////////////
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
