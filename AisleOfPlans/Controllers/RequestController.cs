﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AisleOfPlans.Models;
using AisleOfPlans.View_Models;

namespace AisleOfPlans.Controllers
{
    public class RequestController : Controller
    {
        private LINQ2AIPASPBDDataContext context = new LINQ2AIPASPBDDataContext();

        // GET: Request
        public ActionResult Index( string status)
        {

            //Creating list to store list of catalogues
            IList<ServiceRequestViewModel> listOfRequests = new List<ServiceRequestViewModel>();

            IQueryable<ServiceRequest> query = null;
            IList<ServiceRequest> requests = new List<ServiceRequest>();

            //Creating query to retrieve all lists
            if (Session["ClientID"] != null)
            {
                query = context.ServiceRequests.Where(
                   r => r.CLIENT_ID == int.Parse(Session["ClientID"].ToString()) &&
                   r.REQUEST_STATUS.CompareTo(status) == 0);
                

                requests = query.ToList();
            }
            else
            {
                IList<Item> listOfItems = getListOfItems(int.Parse(Session["VendorID"].ToString()));
                ServiceRequest req;
                foreach (Item i in listOfItems)
                {
                    query = context.ServiceRequests.Where(
                                          request => request.ITEM_ID == i.ITEM_ID &&
                                          request.REQUEST_STATUS.CompareTo(status) == 0);
                    if(query.Count() == 0)
                    {
                        continue;
                    }
                    
                    foreach (ServiceRequest s in query)
                    {
                        req = new ServiceRequest
                        {
                            CLIENT_ID = s.CLIENT_ID,
                            ITEM_ID = s.ITEM_ID,
                            REQUEST_DATE = s.REQUEST_DATE,
                            REQUEST_DETAILS = s.REQUEST_DETAILS,
                            REQUEST_ID = s.REQUEST_ID,
                            REQUEST_STATUS = s.REQUEST_STATUS
                        };


                        requests.Add(req);
                    }
                }
            }

            foreach (var r in requests)
            {
                listOfRequests.Add(new ServiceRequestViewModel
                {
                    WeddingID = context.Weddings.Where(
                        w => w.CLIENT_ID == r.CLIENT_ID).Select(
                        w => w.WEDDING_ID).Single(),
                    Item_ID = r.ITEM_ID,
                    Request_ID = r.REQUEST_ID,
                    Date_Requested = r.REQUEST_DATE,
                    RequestDetails = r.REQUEST_DETAILS,
                    Status = r.REQUEST_STATUS,
                    //Extra stuff for display purposes
                    Image_URL = context.Items.Where(
                        i => i.ITEM_ID == r.ITEM_ID).Single().IMAGE_URL,
                    ItemName = context.Items.Where(
                        i => i.ITEM_ID == r.ITEM_ID).Single().ITEM_NAME
                });
            }

            return View(listOfRequests);
        }

        private IList<Item> getListOfItems(int vendorID)
        {
            IQueryable<Catalogue> listOfCatalogs = getItemCatalogue(vendorID);
            IQueryable<Item> query = null;
            IList<Item> listOfItems = new List<Item>();

            foreach (Catalogue c in listOfCatalogs)
            {
                query = from item in context.Items
                            where item.CATALOGUE_ID == c.CATALOGUE_ID
                            select item;
                foreach (Item i in query)
                {
                    listOfItems.Add(i);
                }
            }
           

            return listOfItems;
        }

        private IQueryable<Catalogue> getItemCatalogue(int vendorID)
        {
            var catalogue = context.Catalogues.Where(
                c => c.VENDOR_ID == vendorID);

            return catalogue;
        }

        private Vendor getItemVendor(Catalogue catalogue)
        {
            Vendor vendor = context.Vendors.Where(
                v => v.VENDOR_ID == catalogue.VENDOR_ID).Single<Vendor>();

            return vendor;
        }

        // GET: Request/Details/5
        public ActionResult Details(int id)
        {
            ServiceRequestViewModel model = context.ServiceRequests.Where(
            request => request.REQUEST_ID == id).Select(
            request => new ServiceRequestViewModel()
            {
                Item_ID = request.ITEM_ID,
                WeddingID = context.Weddings.Where(
                        w => w.CLIENT_ID == request.CLIENT_ID).Select(
                        w => w.WEDDING_ID).Single(),
               
                Date_Requested = request.REQUEST_DATE,
                RequestDetails = request.REQUEST_DETAILS,
                proofLocation = context.PaymentProofs.Where(
                        i => i.REQUEST_ID == request.REQUEST_ID).Select(
                        i => i.PROOF_URL).Single(),
                Status = request.REQUEST_STATUS,
                //Extra stuff for display purposes
                Image_URL = context.Items.Where(
                        i => i.ITEM_ID == request.ITEM_ID).Select(
                        i => i.IMAGE_URL).Single(),
                ItemName = context.Items.Where(
                        i => i.ITEM_ID == request.ITEM_ID).Select(
                        i => i.ITEM_NAME).Single(),
            }).SingleOrDefault();

            return View(model); 
        }

        // GET: Request/Create
        public ActionResult Create(int id)
        {
            ServiceRequestViewModel model = new ServiceRequestViewModel();
            model.Item_ID = id;
            model.ItemType = context.Items.Where(
                i => i.ITEM_ID == id).Single().ITEM_TYPE;

            return View(model);
        }

        // POST: Request/Create
        [HttpPost]
        public ActionResult Create(ServiceRequestViewModel model)
        {
            try
            {
                ServiceRequest request = new ServiceRequest
                {
                    CLIENT_ID = int.Parse(Session.Contents["ClientID"].ToString()),
                    REQUEST_DATE = System.DateTime.Now.ToLocalTime(),
                    REQUEST_STATUS = "Pending",
                    ITEM_ID = model.Item_ID
                };

                StoreDetails(request, model);

                context.ServiceRequests.InsertOnSubmit(request);
                context.SubmitChanges();

                return RedirectToAction("Index", new { status = request.REQUEST_STATUS });
            }
            catch
            {
                return View(model);
            }
        }

        private void StoreDetails(ServiceRequest request, ServiceRequestViewModel model)
        {
            if (model.ItemType.Contains("Attire"))
            {
                request.REQUEST_DETAILS = (model.NumItems + "," +
                    model.HeightMeasurement + ","+
                    model.BustMeasurement + "," +
                    model.WaistMeasurement + "," +
                    model.HipMeasurement + ","+
                    model.FashionDetails);
            }
            else if (model.ItemType.Contains("Shoes"))
            {
                request.REQUEST_DETAILS = (model.NumItems + "," +
                    model.FashionDetails );
            }
            else if(model.ItemType.Contains("Menu"))
            {
                request.REQUEST_DETAILS = (model.NumCateringDays + "," +
                    model.DieteryDetails );
            }
            else if(model.ItemType.Contains("Cake"))
            {
                request.REQUEST_DETAILS = (model.NumCakes + "," +
                    model.DieteryDetails);
            }
            else if(model.ItemType.Contains("Decorations"))
            {
                request.REQUEST_DETAILS = (model.NumPackages + "," +
                    model.ArtDetails);
            }
            else if(model.ItemType.Contains("Hair") || model.ItemType.Contains("Jewellery"))
            {
                request.REQUEST_DETAILS = (model.NumPeople + "," +
                    model.StylingDetails);
            }
            else if(model.ItemType.Contains("Transportation"))
            {
                request.REQUEST_DETAILS = (model.NumVehicles + "," +
                    model.NumVehicleDays + "," +
                    model.VehicleDetails );
            }
            else if(model.ItemType.Contains("Venue"))
            {
                request.REQUEST_DETAILS = (model.NumDays + "," +
                    model.isAccommodationRequired + "," +
                    model.VenueDetails);
            }
            else if(model.ItemType.Contains("Entertainment"))
            {
                request.REQUEST_DETAILS = (model.NumPerformanceHours + "," +
                    model.EntertainmentDetails);
            }
            else if(model.ItemType.Contains("Photographer") || model.ItemType.Contains("Videographer") || model.ItemType.Contains("Artist"))
            {
                request.REQUEST_DETAILS = (model.NumServiceHours + "," +
                    model.MediaDetails);
            }
        }

        public ActionResult Complete(int id)
        {
            ServiceRequestViewModel model = context.ServiceRequests.Where(
            request => request.REQUEST_ID == id).Select(
            request => new ServiceRequestViewModel()
            {
                Item_ID = request.ITEM_ID,
                WeddingID = context.Weddings.Where(
                        w => w.CLIENT_ID == request.CLIENT_ID).Select(
                        w => w.WEDDING_ID).Single(),
                Request_ID = request.REQUEST_ID,
                Status = request.REQUEST_STATUS,
                Date_Requested = request.REQUEST_DATE,
                RequestDetails = request.REQUEST_DETAILS,
                //Extra stuff for display purposes
                Image_URL = context.Items.Where(
                        i => i.ITEM_ID == request.ITEM_ID).Select(
                        i => i.IMAGE_URL).Single(),
                ItemName = context.Items.Where(
                        i => i.ITEM_ID == request.ITEM_ID).Select(
                        i => i.ITEM_NAME).Single(),
            }).SingleOrDefault();

            return View(model);
        }

        [HttpPost]
        public ActionResult Complete(ServiceRequestViewModel model)
        {
            try
            {
                string savePath = null;
                string filePath = null;


                if (model.proof != null)
                {
                    if (model.proof.ContentType.Contains("jpeg") || model.proof.ContentType.Contains("jpg"))
                    {
                        savePath = Server.MapPath("~/Content/proofs/") + model.Request_ID + model.ItemName + ".jpeg";
                        filePath = "../../Content/proofs/" + model.Request_ID + model.ItemName + ".jpeg";
                        model.proof.SaveAs(savePath);
                    }
                    else if (model.proof.ContentType.Contains("png"))
                    {
                        savePath = Server.MapPath("~/Content/proofs/") + model.Request_ID + model.ItemName + ".png";
                        filePath = "../../Content/proofs/" + model.Request_ID + model.ItemName + ".png";
                        model.proof.SaveAs(savePath);
                    }
                    else if (model.proof.ContentType.Contains("pdf"))
                    {
                        savePath = Server.MapPath("~/Content/proofs/") + model.Request_ID + model.ItemName + ".pdf";
                        filePath = "../../Content/proofs/" + model.Request_ID + model.ItemName + ".pdf";
                        model.proof.SaveAs(savePath);
                    }

                }
                else
                {
                    ViewBag.ErrorMessage = "Please make sure you upload a 'pdf'/'jpg'/'png' file.";
                    return View(model);
                }
                ServiceRequest request = context.ServiceRequests.SingleOrDefault(
                    r => r.REQUEST_ID == model.Request_ID);

                request.REQUEST_STATUS = "Complete";

                try
                {
                    PaymentProof proof = new PaymentProof
                    {
                        PROOF_URL = filePath,
                        REQUEST_ID = model.Request_ID,
                    };

                    context.PaymentProofs.InsertOnSubmit(proof);

                    updatePlan(model.WeddingID, model.Item_ID);
                    context.SubmitChanges();
                }
                catch
                {
                    ViewBag.ErrorMessage = "File upload Failed. Please try again";
                    return View(model);
                }



                // notify(request.VendorID);

                return RedirectToAction("Index", new { status = request.REQUEST_STATUS });
            }
            catch
            {
                return View(model);
            }
        }

        public ActionResult Decline(int id)
        {
            try
            {
                ServiceRequest request = context.ServiceRequests.SingleOrDefault(
                r => r.REQUEST_ID == id);

                request.REQUEST_STATUS = "Declined";

                /////////////////////////Reason for declining////////////////////

                context.SubmitChanges();

                // notify(request.CLIENT_ID);

                return RedirectToAction("Index", new { status = request.REQUEST_STATUS });
            }
            catch
            {
                return View();
            }
        }

   
        public ActionResult Accept(int id)
        {
            try
            {
                ServiceRequest request = context.ServiceRequests.SingleOrDefault(
                                        r => r.REQUEST_ID == id);

                request.REQUEST_STATUS = "Accepted";

                context.SubmitChanges();

                // notify(request.CLIENT_ID);

                return RedirectToAction("Index", new { status = request.REQUEST_STATUS });
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Cancel(int id)
        {
            try
            {
                ServiceRequest request = context.ServiceRequests.SingleOrDefault(
                                        r => r.REQUEST_ID == id);

                request.REQUEST_STATUS = "Canceled";

                context.SubmitChanges();

                // notify(request.VendorID);

                return RedirectToAction("Index", new { status = request.REQUEST_STATUS });
            }
            catch
            {
                return View();
            }
        }

        /*
         * Function for updating  related plans and plan objectives
         * */
        private void updatePlan(int weddingID, int itemID) 
        {
            try
            {
                Item item = context.Items.Where(
                    i => i.ITEM_ID == itemID).Single<Item>();

                var plans = context.Plans.Where(
                    p => p.WEDDING_ID == weddingID);

                Plan plan = null;
                PlanObjective objective = null;
                foreach (Plan p in plans)
                {
                    var o = from obj in context.PlanObjectives
                                where obj.PLAN_ID == p.PLAN_ID && obj.OBJECTIVE_NAME.CompareTo(item.ITEM_TYPE) == 0
                                select obj;

                    if(o.Count() >0)
                    {
                        objective = o.SingleOrDefault<PlanObjective>();
                        plan = context.Plans.Where(
                            i => i.PLAN_ID == p.PLAN_ID).Single<Plan>();
                        break;
                    }
                }

               
                objective.OBJECTIVE_COMPLETE = "True";

                context.SubmitChanges();
                context.Refresh(System.Data.Linq.RefreshMode.KeepChanges,context.PlanObjectives);

                plan.EST_COST += item.ITEM_PRICE;

                Budget budget = context.Budgets.Where(
                    b => b.BUDGET_ID == b.BUDGET_ID).Single<Budget>();

                budget.BUDGET_USED += (int)plan.EST_COST;

                double totalObjectives =  context.PlanObjectives.Where(
                        o => o.PLAN_ID == plan.PLAN_ID & o.OBJECTIVE_STATUS.CompareTo("Enabled") == 0).Count()/1.0;

                double completeObjectives = context.PlanObjectives.Where(
                        o => o.PLAN_ID == plan.PLAN_ID &&
                        o.OBJECTIVE_COMPLETE.CompareTo("True") == 0).Count()*1.0;

                double percentComplete = (completeObjectives/totalObjectives);

                plan.AMOUNT_COMPLETE = (int)(percentComplete*100);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
