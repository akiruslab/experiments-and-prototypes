﻿using AisleOfPlans.Models;
using AisleOfPlans.View_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AisleOfPlans.Controllers
{
    public class ProfileController : Controller
    {
        private LINQ2AIPASPBDDataContext context = new LINQ2AIPASPBDDataContext();

        public ActionResult Index()
        {
            if((Session["ClientID"] != null))
                return RedirectToAction("LoadClientProfile");
            else if((Session["VendorID"] != null))
                return RedirectToAction("LoadVendorProfile");

            return RedirectToAction("Login", "Account");
        }

        // GET: Profile
        public ActionResult LoadClientProfile()
        {
            if(Session["UserID"]== null )
            {
                return RedirectToAction("Login", "Account");
            }
            else if(Session["ClientID"] == null)
            {
                return RedirectToAction("LoadVendorProfile");
            }

            ClientViewModel model = context.Clients.Where(
                  client => client.USER_ID == Session["UserID"].ToString()).Select(
                  client => new ClientViewModel()
                  {
                      User_ID = client.USER_ID,
                      Client_ID = client.CLIENT_ID,
                      Lastname = client.LASTNAME,
                      Partner1_Name = client.PARTNER_1_NAME,
                      Partner2_Name = client.PARTNER_2_NAME,
                      Pic_Location = client.PIC_LOCATION,
                      ClientWedding = context.Weddings.Where(
                          w => w.CLIENT_ID == client.CLIENT_ID).Single<Wedding>(),
                      
                  }).Single();

            var query = from plan in context.Plans
                        where plan.WEDDING_ID == model.ClientWedding.WEDDING_ID
                        select plan;

            //Setting up plans
            model.listOfPlans = new List<PlanViewModel>();

            foreach (Plan p in query)
            {
                model.listOfPlans.Add(new PlanViewModel
                {
                    Plan_ID = p.PLAN_ID,
                    Wedding_ID = p.WEDDING_ID,
                    Amount_Complete = p.AMOUNT_COMPLETE,
                    Deadline = p.DEADLINE,
                    PlanType_ID = p.PLAN_TYPE_ID,
                    Priority_level = p.PRIORITY_LEVEL,
                    Status = p.PLAN_STATUS,
                    Cost =(int) p.EST_COST,
                    Plan_Name = context.PlanTypes.Where(
                        type => type.PLAN_TYPE_ID == p.PLAN_TYPE_ID).Single().TYPE_NAME
                });
            }
            //Setting up budget
            model.budget = context.Budgets.Where(
                          b => b.WEDDING_ID == model.ClientWedding.WEDDING_ID).Single<Budget>();

            Session["ClientID"] = model.Client_ID;

            try
            { 
                Session["WeddingID"] = model.ClientWedding.WEDDING_ID;
            }
            catch 
            {
                return RedirectToAction("Create", "Wedding");
            }

     

            return View(model);
        }

        // GET: Profile
        public ActionResult LoadVendorProfile()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else if (Session["VendorID"] == null)
            {
                return RedirectToAction("LoadClientProfile");
            }
            VendorViewModel model = context.Vendors.Where(
                              vendor => vendor.USER_ID == (
                              (Session["UserID"].ToString()))).Select(
                              vendor => new VendorViewModel()
                              {
                                  User_ID = vendor.USER_ID,
                                  Vendor_ID = vendor.VENDOR_ID,
                                  BankingDetails = vendor.BANKING_DETAILS,
                                  Business_Name = vendor.BUSINESS_NAME,
                                  Description = vendor.VENDOR_DESCRIPTION,
                                  Logo_Location = vendor.LOGO_LOCATION,
                                  Web_Address = vendor.WEB_ADDRESS,
                                  Type = vendor.TYPE,
                                  approxEarnings = new VendorReportViewModel(),
                                  
                              }).Single();

            var cats = context.Catalogues.Where(
                                      c => c.VENDOR_ID == model.Vendor_ID);

            ///////////////////////////*Calcualtions for most popular catalogue and item*/////////////////////////////////////////

            int highestForCat = 0;
            int currentCatTotal = 0;
            int highestForItem = 0;
            int currentItemTotal = 0;

            Catalogue mostPopCatalogue = null;
            Item mostPopItem = null;

            string[] requestDetails = null;

            model.NumCatalogues = cats.Count();

            if(cats.Count()>0)
            {
                foreach (Catalogue c in cats)
                {
                    var items = context.Items.Where(
                         i => i.CATALOGUE_ID == c.CATALOGUE_ID);

                    model.NumItems += items.Count();

                    if (items.Count() > 0)
                    {
                        currentCatTotal = 0;
                        foreach (Item i in items)
                        {
                            var itemsRequested = context.ServiceRequests.Where(
                                s => s.ITEM_ID == i.ITEM_ID && s.REQUEST_STATUS.CompareTo("Complete") == 0);

                            if (itemsRequested.Count() > 0)
                            {
                                currentItemTotal = 0;
                                foreach (ServiceRequest s in itemsRequested)
                                {
                                    requestDetails = s.REQUEST_DETAILS.Split(',');
                                    currentItemTotal += i.ITEM_PRICE * (int.Parse(requestDetails[0]));

                                    model.approxEarnings.TotalEstEarnings += currentItemTotal;

                                    if(highestForItem < currentItemTotal)
                                    {
                                        highestForItem = currentItemTotal;
                                        mostPopItem = i;
                                    }
                                }//end for requests
                                currentCatTotal += currentItemTotal;
                            }//end if
                        }//end for items

                        if (highestForCat < currentCatTotal)
                        {
                            highestForCat = currentCatTotal;
                            mostPopCatalogue = c;
                        }
                    }//end if

                }//end for catalogues
            }//end if

            model.approxEarnings.MostEarningCatalog = mostPopCatalogue;
            model.approxEarnings.HighestCatEarnings = highestForCat;
            model.approxEarnings.MostEarningItem = mostPopItem;
            model.approxEarnings.HighestItemEarnings = highestForItem;

            return View(model);
        }

        // GET: Profile/Create
        public ActionResult CreateClientProfile()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Account");
            }

            ClientViewModel model = context.Clients.Where(
                 client => client.USER_ID == (Session["UserID"].ToString())).Select(
                 client => new ClientViewModel()
                 {
                     User_ID = client.USER_ID,
                     Client_ID = client.CLIENT_ID,
                     Lastname = client.LASTNAME,
                     Partner1_Name = client.PARTNER_1_NAME,
                     Partner2_Name = client.PARTNER_2_NAME,
                     Pic_Location = client.PIC_LOCATION,
                 }).Single();

            Session["ClientID"] = model.Client_ID;
            return View(model);
        }

        private void updateUser(string id)
        {
            try
            {
                AspNetUser user = context.AspNetUsers.Where(
                    u => u.Id == id).SingleOrDefault<AspNetUser>();

                user.IsSetup = "true";

                context.SubmitChanges();
            }
            catch (Exception e)
            {
                ViewBag.Error(e.StackTrace);
            }
        }

        // POST: Profile/Create
        [HttpPost]
        public ActionResult CreateClientProfile(ClientViewModel model)
        {
            try
            {
                string savePath = null;
                string filePath = null;


                if (model.Image != null)
                {
                    if (model.Image.ContentType.Contains("jpeg") || model.Image.ContentType.Contains("jpg"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.Client_ID + model.Lastname + ".jpeg";
                        filePath = "../../Content/images/" + model.Client_ID + model.Lastname + ".jpeg";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("png"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.Client_ID + model.Lastname + ".png";
                        filePath = "../../Content/images/" + model.Client_ID + model.Lastname + ".png";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("bmp"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.Client_ID + model.Lastname + ".bmp";
                        filePath = "../../Content/images/" + model.Client_ID + model.Lastname + ".bmp";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("gif"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.Client_ID + model.Lastname + ".gif";
                        filePath = "../../Content/images/" + model.Client_ID + model.Lastname + ".gif";
                        model.Image.SaveAs(savePath);
                    }
                    else
                    {
                        filePath = "../../Content/images/defaultClient.jpg";
                    }

                }
                else
                {
                    filePath = "../../Content/images/defaultClient.jpg";
                }

                Client client = context.Clients.Where(
                    c => c.CLIENT_ID ==int.Parse(Session["ClientID"].ToString())).Single<Client>();

                client.USER_ID = Session["UserID"].ToString();
                client.LASTNAME = model.Lastname;
                client.PARTNER_1_NAME = model.Partner1_Name;
                client.PARTNER_2_NAME = model.Partner2_Name;
                client.PIC_LOCATION = filePath;

                context.SubmitChanges();

                //Update user
                updateUser(client.USER_ID);

                return RedirectToAction("Create", "Wedding");
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Profile/Create
        public ActionResult CreateVendorProfile()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Account");
            }
        
            VendorViewModel model = context.Vendors.Where(
                              vendor => vendor.USER_ID == (
                              (Session["UserID"].ToString()))).Select(
                              vendor => new VendorViewModel()
                              {
                                  User_ID = vendor.USER_ID,
                                  Vendor_ID = vendor.VENDOR_ID,
                                  BankingDetails = vendor.BANKING_DETAILS,
                                  Business_Name = vendor.BUSINESS_NAME,
                                  Description = vendor.VENDOR_DESCRIPTION,
                                  Logo_Location = vendor.LOGO_LOCATION,
                                  Web_Address = vendor.WEB_ADDRESS,
                                  Type = vendor.TYPE

                              }).Single();

            Session["VendorID"] = model.Vendor_ID;

            return View(model);
        }

        // POST: Profile/Create
        [HttpPost]
        public ActionResult CreateVendorProfile(VendorViewModel model)
        {
            try
            {
                string savePath = null;
                string filePath = null;


                if (model.Image != null)
                {
                    if (model.Image.ContentType.Contains("jpeg") || model.Image.ContentType.Contains("jpg"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.Vendor_ID + model.Business_Name + ".jpeg";
                        filePath = "../../Content/images/" + model.Vendor_ID + model.Business_Name + ".jpeg";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("png"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.Vendor_ID + model.Business_Name + ".png";
                        filePath = "../../Content/images/" + model.Vendor_ID + model.Business_Name + ".png";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("bmp"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.Vendor_ID + model.Business_Name + ".bmp";
                        filePath = "../../Content/images/" + model.Vendor_ID + model.Business_Name + ".bmp";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("gif"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.Vendor_ID + model.Business_Name + ".gif";
                        filePath = "../../Content/images/" + model.Vendor_ID + model.Business_Name + ".gif";
                        model.Image.SaveAs(savePath);
                    }
                    else
                    {
                        filePath = "../../Content/images/defaultVendor.gif";
                    }

                }
                else
                {
                    filePath = "../../Content/images/defaultVendor.gif";
                }

                Vendor vendor = context.Vendors.Where(
                   c => c.VENDOR_ID == int.Parse(Session["VendorID"].ToString())).Single<Vendor>();

                vendor.BANKING_DETAILS = model.BankingDetails;
                vendor.BUSINESS_NAME = model.BankingDetails;
                vendor.LOGO_LOCATION = filePath;
                vendor.TYPE = model.Type;
                vendor.VENDOR_DESCRIPTION = model.Description;
                vendor.WEB_ADDRESS = model.Web_Address;
                vendor.USER_ID = Session["UserID"].ToString();

                context.SubmitChanges();

                //Update user
                updateUser(vendor.USER_ID);

                return RedirectToAction("LoadVendorProfile", "Profile");
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Profile/Edit/5
        public ActionResult EditClientProfile()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else if (Session["ClientID"] == null)
            {
                return RedirectToAction("LoadVendorProfile");
            }

            ClientViewModel model = context.Clients.Where(
                 client => client.USER_ID == (Session["UserID"].ToString())).Select(
                 client => new ClientViewModel()
                 {
                     User_ID = client.USER_ID,
                     Client_ID = client.CLIENT_ID,
                     Lastname = client.LASTNAME,
                     Partner1_Name = client.PARTNER_1_NAME,
                     Partner2_Name = client.PARTNER_2_NAME,
                     Pic_Location = client.PIC_LOCATION,
                     ClientWedding = context.Weddings.Where(
                         w => w.CLIENT_ID == client.CLIENT_ID).Single()
                 }).Single();

            return View(model);
        }

        // POST: Profile/Edit/5
        [HttpPost]
        public ActionResult EditClientProfile(ClientViewModel model)
        {
            try
            {
                string savePath = null;
                string filePath = null;


                if (model.Image != null)
                {
                    if (model.Image.ContentType.Contains("jpeg") || model.Image.ContentType.Contains("jpg"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.Client_ID + model.Lastname + ".jpeg";
                        filePath = "../../Content/images/" + model.Client_ID + model.Lastname + ".jpeg";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("png"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.Client_ID + model.Lastname + ".png";
                        filePath = "../../Content/images/" + model.Client_ID + model.Lastname + ".png";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("bmp"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.Client_ID + model.Lastname + ".bmp";
                        filePath = "../../Content/images/" + model.Client_ID + model.Lastname + ".bmp";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("gif"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.Client_ID + model.Lastname + ".gif";
                        filePath = "../../Content/images/" + model.Client_ID + model.Lastname + ".gif";
                        model.Image.SaveAs(savePath);
                    }
                    else
                    {
                        filePath = "../../Content/images/defaultClient.jpg";
                    }

                }
                else
                {
                    filePath = "../../Content/images/defaultClient.jpg";
                }

                Client client = context.Clients.Where(
                    c => c.CLIENT_ID == model.Client_ID).Single<Client>();

                client.LASTNAME = model.Lastname;
                client.PARTNER_1_NAME = model.Partner1_Name;
                client.PARTNER_2_NAME = model.Partner2_Name;
                client.PIC_LOCATION = filePath;

                context.SubmitChanges();

                return RedirectToAction("LoadClientProfile");
            }
            catch
            {
                return View(model);
            }
        }

        public ActionResult EditVendorProfile()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else if (Session["VendorID"] == null)
            {
                return RedirectToAction("LoadClientProfile");
            }

            VendorViewModel model = context.Vendors.Where(
                              vendor => vendor.USER_ID == (
                              (Session["UserID"].ToString()))).Select(
                              vendor => new VendorViewModel()
                              {
                                  User_ID = vendor.USER_ID,
                                  Vendor_ID = vendor.VENDOR_ID,
                                  BankingDetails = vendor.BANKING_DETAILS,
                                  Business_Name = vendor.BUSINESS_NAME,
                                  Description = vendor.VENDOR_DESCRIPTION,
                                  Logo_Location = vendor.LOGO_LOCATION,
                                  Web_Address = vendor.WEB_ADDRESS,
                                  Type = vendor.TYPE

                              }).Single();

            return View(model);
        }

        // POST: Profile/Edit/5
        [HttpPost]
        public ActionResult EditVendorProfile(VendorViewModel model)
        {
            try
            {
                string savePath = null;
                string filePath = null;


                if (model.Image != null)
                {
                    if (model.Image.ContentType.Contains("jpeg") || model.Image.ContentType.Contains("jpg"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.Vendor_ID + model.Business_Name + ".jpeg";
                        filePath = "../../Content/images/" + model.Vendor_ID + model.Business_Name + ".jpeg";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("png"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.Vendor_ID + model.Business_Name + ".png";
                        filePath = "../../Content/images/" + model.Vendor_ID + model.Business_Name + ".png";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("bmp"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.Vendor_ID + model.Business_Name + ".bmp";
                        filePath = "../../Content/images/" + model.Vendor_ID + model.Business_Name + ".bmp";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("gif"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.Vendor_ID + model.Business_Name + ".gif";
                        filePath = "../../Content/images/" + model.Vendor_ID + model.Business_Name + ".gif";
                        model.Image.SaveAs(savePath);
                    }
                    else
                    {
                        filePath = "../../Content/images/defaultVendor.gif";
                    }

                }
                else
                {
                    filePath = "../../Content/images/defaultVendor.gif";
                }

                Vendor vendor = context.Vendors.Where(
                    c => c.VENDOR_ID == model.Vendor_ID).Single<Vendor>();

                vendor.BANKING_DETAILS = model.BankingDetails;
                vendor.BUSINESS_NAME = model.Business_Name;
                vendor.LOGO_LOCATION = filePath;
                vendor.TYPE = model.Type;
                vendor.VENDOR_DESCRIPTION = model.Description;
                vendor.WEB_ADDRESS = model.Web_Address;

                context.SubmitChanges();

                return RedirectToAction("LoadVendorProfile");
            }
            catch
            {
                return View(model);
            }
        }
    }
}
