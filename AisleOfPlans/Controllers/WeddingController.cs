﻿using AisleOfPlans.Models;
using AisleOfPlans.View_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AisleOfPlans.Controllers
{
    public class WeddingController : Controller
    {
        private LINQ2AIPASPBDDataContext context = new LINQ2AIPASPBDDataContext();

        // GET: Wedding
        public ActionResult Index()
        {
            if (Session.Contents["ClientID"] == null)
            {
                RedirectToAction("LoadClientProfile", "Profile");
            }

            WeddingViewModel model = context.Weddings.Where(
                wed => wed.CLIENT_ID == (int.Parse(Session["ClientID"].ToString()))).Select(
                wed => new WeddingViewModel()
                {
                    Wedding_ID = wed.WEDDING_ID,
                    Client_ID = wed.CLIENT_ID,
                    Status = wed.STATUS,
                    Theme = wed.THEME,
                    WeddingDate = wed.WEDDING_DATE,
                   
                }).SingleOrDefault();

            string[] address = context.Weddings.Where(
              wed => wed.WEDDING_ID == model.Wedding_ID).Single().LOCATION.Split(',');

            model.Street = address[0];
            model.Suburb = address[1];
            model.City = address[2];
            model.Province = address[3];
            model.Country = address[4];

            //Setting clientViewModel

            ClientViewModel cModel = context.Clients.Where(
                c => c.CLIENT_ID == model.Client_ID).Select(
                c => new ClientViewModel()
                {
                    Lastname = c.LASTNAME,
                    Partner1_Name = c.PARTNER_1_NAME,
                    Partner2_Name = c.PARTNER_2_NAME,
                    Pic_Location = c.PIC_LOCATION
                }).Single();

            model.Client = cModel;

            return View(model);

        }

        // GET: Wedding/Create
        public ActionResult Create()
        {
            if(Session.Contents["ClientID"] == null)
            {
                RedirectToAction("Login", "Account");
            }

            WeddingViewModel model = new WeddingViewModel();
            
            return View(model);
        }

        // POST: Wedding/Create
        [HttpPost]
        public ActionResult Create(WeddingViewModel model)
        {
            try
            {
                Wedding wed = new Wedding()
                {
                    CLIENT_ID = int.Parse(Session.Contents["ClientID"].ToString()),
                    THEME = model.Theme,
                    WEDDING_DATE = model.WeddingDate,
                    STATUS = "On",
                    LOCATION = model.Street + "," + model.Suburb + "," + model.City + "," + model.Province + "," + model.Country
                };

                context.Weddings.InsertOnSubmit(wed);
                context.SubmitChanges();

                Budget budget = new Budget()
                {
                    BUDGET_AVAILABLE = model.budget.Budget_Available,
                    BUDGET_USED = 0,
                    WEDDING_ID = wed.WEDDING_ID
                };

                context.Budgets.InsertOnSubmit(budget);

                Session["WeddingID"] = wed.WEDDING_ID;
                createPlans(wed);

                return RedirectToAction("Index", new { id = Session.Contents["ClientID"] });
            }
            catch
            {
                return View(model);
            }
        }

        private void createPlans(Wedding wed)
        {
            IList<Plan> listOfPlans = new List<Plan>();

            //Creating query to retrieve all plan
            var query = from planType in context.PlanTypes
                        select planType;

            var planTypes = query.ToList();

            foreach (var p in planTypes)
            {
                listOfPlans.Add(new Plan
                {
                    PLAN_STATUS = "Activated",
                    AMOUNT_COMPLETE = 0,
                    DEADLINE = wed.WEDDING_DATE,
                    PRIORITY_LEVEL = 10,
                    WEDDING_ID = wed.WEDDING_ID,
                    PLAN_TYPE_ID = p.PLAN_TYPE_ID
                });
            }
            context.Plans.InsertAllOnSubmit(listOfPlans);
            context.SubmitChanges();

            creatObjectives(listOfPlans);
        }

        private void creatObjectives(IList<Plan> listOfPlans)
        {
            IList<PlanObjective> listOfObjectives = new List<PlanObjective>();

            //Creating query to retrieve all plan
            var query = from objective in context.Objectives
                        select objective;

            var objectives = query.ToList();

            foreach (var p in listOfPlans)
            {
                foreach (var o in objectives)
                {
                    if(p.PLAN_TYPE_ID == o.PLAN_TYPE_ID)
                    {//if the current objective has the same plan type
                     //as the current plan, then add a plan-objective
                     //linked to that plan
                        listOfObjectives.Add(new PlanObjective
                        {
                            OBJECTIVE_COMPLETE = "False",
                            OBJECTIVE_STATUS = "Enabled",
                            PLAN_ID = p.PLAN_ID,
                            OBJECTIVE_NAME = o.OBJECTIVE_NAME,
                        });
                    }  
                }
            }

            context.PlanObjectives.InsertAllOnSubmit(listOfObjectives);

            context.SubmitChanges();
        }

        // GET: Wedding/Edit/5
        public ActionResult Edit(int WeddingID)
        {
            string[] address = context.Weddings.Where(
                wed => wed.WEDDING_ID == WeddingID).Single().LOCATION.Split(',');

            WeddingViewModel model = context.Weddings.Where(
                wed => wed.WEDDING_ID == WeddingID).Select(
                wed => new WeddingViewModel()
                {
                    Wedding_ID = wed.WEDDING_ID,
                    Client_ID = wed.CLIENT_ID,
                    Status = wed.STATUS,
                    Theme = wed.THEME,
                    WeddingDate = wed.WEDDING_DATE,
                    //Address:
                    Street = address[0],
                    Suburb = address[1],
                    City = address[2],
                    Province = address[3],
                    Country = address[4]
                }).SingleOrDefault();

            BudgetViewModel budget = context.Budgets.Where(
                       b => b.WEDDING_ID == model.Wedding_ID).Select(
                b => new BudgetViewModel()
                {
                    Budget_Available = b.BUDGET_AVAILABLE,
                    Budget_ID = b.BUDGET_ID
                }).Single();

            model.budget = budget;

            return View(model);
        }

        
        [HttpPost]
        public ActionResult Edit(WeddingViewModel model)
        {
            try
            {
                Wedding wed = context.Weddings.Where(
                     w => w.WEDDING_ID == model.Wedding_ID).Single<Wedding>();

                wed.THEME = model.Theme;
                wed.LOCATION = model.Street + "," + model.Suburb + "," + model.City + "," + model.Province + "," + model.Country;
                wed.WEDDING_DATE = model.WeddingDate;

                Budget budget = context.Budgets.Where(
                    b => b.BUDGET_ID == model.budget.Budget_ID).Single<Budget>();

                budget.BUDGET_AVAILABLE = model.budget.Budget_Available;

                context.SubmitChanges();

                return RedirectToAction("Index", new { id = Session.Contents["ClientID"] });
            }
            catch
            {
                return View(model);
            }
        }

        // POST: Wedding/Delete/5
        [HttpPost]
        public ActionResult Cancel(int id)
        {
            try
            {
                Wedding wed = context.Weddings.Where(
                    w => w.WEDDING_ID == id).Single<Wedding>();

                wed.STATUS = "Canceled";

                return RedirectToAction("Index", new { id = Session.Contents["ClientID"] });
            }
            catch
            {
                return View();
            }
        }
    }

    public class PlanController : Controller
    {
        private LINQ2AIPASPBDDataContext context = new LINQ2AIPASPBDDataContext();

        // GET: Wedding
        public ActionResult Index()
        {
            if (Session.Contents["WeddingID"] == null)
            {
                RedirectToAction("Create", "Wedding");
            }
            //Creating list to store list of catalogues
            IList<PlanViewModel> listOfPlans = new List<PlanViewModel>();

            //Creating query to retrieve all lists
            var query = from plan in context.Plans
                        where plan.WEDDING_ID == int.Parse( Session["WeddingID"].ToString())
                        select plan;

            var plans = query.ToList();

            foreach (var p in plans)
            {
                listOfPlans.Add(new PlanViewModel
                {
                    Plan_ID = p.PLAN_ID,
                    Wedding_ID = p.WEDDING_ID,
                    Amount_Complete = p.AMOUNT_COMPLETE,
                    Deadline = p.DEADLINE,
                    PlanType_ID = p.PLAN_TYPE_ID,
                    Priority_level = p.PRIORITY_LEVEL,
                    Status = p.PLAN_STATUS,
                    Plan_Name = context.PlanTypes.Where(
                        type => type.PLAN_TYPE_ID == p.PLAN_TYPE_ID).Single().TYPE_NAME
                });
            }

            return View(listOfPlans);
        }

        // GET: Wedding/Edit/5
        public ActionResult Edit(int id)
        {
            PlanViewModel model = context.Plans.Where(
                p => p.PLAN_ID == id).Select(
                p => new PlanViewModel()
                {
                    Plan_ID = p.PLAN_ID,
                    PlanType_ID = p.PLAN_TYPE_ID,
                    Amount_Complete = p.AMOUNT_COMPLETE,
                    Deadline = p.DEADLINE,
                    Priority_level = p.PRIORITY_LEVEL,
                    Status = p.PLAN_STATUS,
                    Wedding_ID = p.WEDDING_ID
                }).SingleOrDefault();

            return View(model);
        }

        // POST: Wedding/Edit/5
        [HttpPost]
        public ActionResult Edit(PlanViewModel model)
        {
            try
            {
                //Retrieving plan to update
                Plan plan = context.Plans.Where(
                     p => p.PLAN_ID == model.Plan_ID).Single<Plan>();

                plan.PLAN_STATUS = model.Status;
                plan.PRIORITY_LEVEL = model.Priority_level;
                plan.DEADLINE = model.Deadline;

                context.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }

  
        public PartialViewResult Cancel(int id)
        {
            try
            {
                ClientViewModel model = (ClientViewModel)Session["model"];
                //Retrieving objective to update
                Plan plan = context.Plans.Where(
                       p => p.PLAN_ID == id).Single<Plan>();

                plan.PLAN_STATUS = "Deactivated";

                context.SubmitChanges();

                foreach(PlanViewModel p in model.listOfPlans)
                {
                    if(p.Plan_ID == plan.PLAN_ID)
                    {
                        p.Status = plan.PLAN_STATUS;
                        break;
                    }
                }

                //Update completion total

                return PartialView("_PlanPartial", model);
            }
            catch
            {
                return PartialView();
            }
        }


        public PartialViewResult Activate(int id)
        {
            try
            {
                ClientViewModel model = (ClientViewModel)Session["model"];
                //Retrieving objective to update
                Plan plan = context.Plans.Where(
                       p => p.PLAN_ID == id).Single<Plan>();

                plan.PLAN_STATUS = "Activate";
                
                context.SubmitChanges();

                foreach (PlanViewModel p in model.listOfPlans)
                {
                    if (p.Plan_ID == plan.PLAN_ID)
                    {
                        p.Status = plan.PLAN_STATUS;
                        break;
                    }
                }
                //Update completion total
               

                return PartialView("_PlanPartial",model);
            }
            catch
            {
                return PartialView();
            }
        }
    }

    public class PlanObjectiveController : Controller
    {
        private LINQ2AIPASPBDDataContext context = new LINQ2AIPASPBDDataContext();

        // GET: Wedding
        public ActionResult Index(int id)
        {
            Session["PlanID"] = id;
            //Creating list to store list of catalogues
            IList<PlanObjectiveViewModel> listOfObjectives = new List<PlanObjectiveViewModel>();

            //Creating query to retrieve all lists
            var query = from objective in context.PlanObjectives
                        where objective.PLAN_ID == id
                        select objective;

            var objectives = query.ToList();

            foreach (var o in objectives)
            {
                listOfObjectives.Add(new PlanObjectiveViewModel
                {
                    Plan_ID = o.PLAN_ID,
                    isComplete = o.OBJECTIVE_COMPLETE,
                    Name = o.OBJECTIVE_NAME,
                    Objective_ID = o.OBJECTIVE_ID,
                    Status = o.OBJECTIVE_STATUS
                });
                
            }

            return View(listOfObjectives);
        }

        public ActionResult Enable(int id)
        {
            try
            {
                //Retrieving objective to update
                PlanObjective objective = context.PlanObjectives.Where(
                     o => o.OBJECTIVE_ID == id ).Single<PlanObjective>();

                objective.OBJECTIVE_STATUS = "Enabled";

                context.SubmitChanges();

                return RedirectToAction("Index/" + Session.Contents["PlanID"].ToString());
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Disable(int id)
        {
            try
            {
                //Retrieving objective to update
                PlanObjective objective = context.PlanObjectives.Where(
                     o => o.OBJECTIVE_ID == id).Single<PlanObjective>();

                objective.OBJECTIVE_STATUS = "Disabled";

                context.SubmitChanges();

                return RedirectToAction("Index/" + Session.Contents["PlanID"].ToString());
            }
            catch
            {
                return View();
            }
        }
    }
}
