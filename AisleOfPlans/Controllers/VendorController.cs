﻿using AisleOfPlans.Models;
using AisleOfPlans.View_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AisleOfPlans.Controllers
{
    public class CatalogController : Controller
    {
        private LINQ2AIPASPBDDataContext context;

        private object loadListOfVendors(string objective)
        {
            IList<VendorViewModel> listOfVendors = new List<VendorViewModel>();

            var query = from vendor in context.Vendors
                        where vendor.TYPE.CompareTo(objective) == 0
                        select vendor;

            var vendors = query.ToList();

            foreach (var i in vendors)
            {
                listOfVendors.Add(new VendorViewModel
                {
                    Vendor_ID = i.VENDOR_ID,
                    Type = i.TYPE,
                    Logo_Location = i.LOGO_LOCATION,
                    Web_Address = i.WEB_ADDRESS,
                    Description = i.VENDOR_DESCRIPTION,
                    Business_Name = i.BUSINESS_NAME
                });
            }

            return listOfVendors;
        }

        private object loadListOfCatalogues(string objective)
        {
            IList<CatalogueViewModel> listOfCatalogues = new List<CatalogueViewModel>();

            var listOfVendors = from vendors in context.Vendors
                                where vendors.TYPE.CompareTo(objective) == 0
                                select vendors;

            IEnumerable<Catalogue> query = null;
            foreach (var v in listOfVendors)
            {
                query = from catalogue in context.Catalogues
                        where catalogue.VENDOR_ID == v.VENDOR_ID
                        select catalogue;
            }

            var catalogues = query.ToList();

            foreach (var c in catalogues)
            {
                listOfCatalogues.Add(new CatalogueViewModel
                {
                    ID = c.CATALOGUE_ID,
                    VendorID = c.VENDOR_ID,
                    Title = c.CAT_TITLE,
                    Description = c.CAT_DESCRIPTION
                });
            }

            return listOfCatalogues;
        }

        public CatalogController()
        {
            context = new LINQ2AIPASPBDDataContext();
        }

        // GET: list of catalogues
        public ActionResult Index()
        {
            int id = int.Parse(Session["VendorID"].ToString());
            //Creating list to store list of catalogues
            IList<CatalogueViewModel> listOfCatalogues = new List<CatalogueViewModel>();

            //Creating query to retrieve all lists
            var query = from catalogue in context.Catalogues
                        where catalogue.VENDOR_ID == id
                        select catalogue;

            var catalogues = query.ToList();

           foreach(var c in catalogues)
            {
                listOfCatalogues.Add(new CatalogueViewModel
                {
                    ID = c.CATALOGUE_ID,
                    VendorID = c.VENDOR_ID,
                    Title = c.CAT_TITLE,
                    Description = c.CAT_DESCRIPTION,
                    ImageUrl = c.CAT_BANNER
                });
            }

            return View(listOfCatalogues);
        }

        // GET: Catalogue/Create
        public ActionResult Create()
        {
            CatalogueViewModel model = new CatalogueViewModel();
            return View(model);
        }

        // POST: Catalogue/Create
        [HttpPost]
        public ActionResult Create(CatalogueViewModel model)
        {
            try
            {
                string savePath = null;
                string filePath = null;

                if(model.Image != null)
                {
                    if (model.Image.ContentType.Contains("jpeg") || model.Image.ContentType.Contains("jpg"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.VendorID + model.Title + ".jpeg";
                        filePath = "../../Content/images/" + model.VendorID + model.Title + ".jpeg";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("png"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.VendorID + model.Title + ".png";
                        filePath = "../../Content/images/" + model.VendorID + model.Title + ".png";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("bmp"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.VendorID + model.Title + ".bmp";
                        filePath = "../../Content/images/" + model.VendorID + model.Title + ".bmp";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("gif"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.VendorID + model.Title + ".gif";
                        filePath = "../../Content/images/" + model.VendorID + model.Title + ".gif";
                        model.Image.SaveAs(savePath);
                    }
                    else
                    {
                        filePath = "../../Content/images/defaultCatalogue.png";
                    }

                }
                else
                {
                    filePath = "../../Content/images/defaultCatalogue.png";
                }

                Catalogue cat = new Catalogue()
                {
                    CAT_TITLE = model.Title,
                    CAT_DESCRIPTION = model.Description,
                    CAT_BANNER = filePath,
                    VENDOR_ID = int.Parse(Session.Contents["VendorID"].ToString()),
                };
                context.Catalogues.InsertOnSubmit(cat);
                context.SubmitChanges();

               return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Catalogue/Edit/5
        public ActionResult Edit(int id)
        {
            CatalogueViewModel model = context.Catalogues.Where(
                cat => cat.CATALOGUE_ID == id).Select(
                cat => new CatalogueViewModel()
                {
                    ID = cat.CATALOGUE_ID,
                    Title = cat.CAT_TITLE,
                    ImageUrl = cat.CAT_BANNER,
                    Description = cat.CAT_DESCRIPTION
                    
                }).SingleOrDefault();

            return View(model);
        }

        // POST: Catalogue/Edit/5
        [HttpPost]
        public ActionResult Edit(CatalogueViewModel model)
        {
            try
            {
                string savePath = null;
                string filePath = null;


                if (model.Image != null)
                {
                    if (model.Image.ContentType.Contains("jpeg") || model.Image.ContentType.Contains("jpg"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.VendorID + model.Title + ".jpeg";
                        filePath = "../../Content/images/" + model.VendorID + model.Title + ".jpeg";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("png"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.VendorID + model.Title + ".png";
                        filePath = "../../Content/images/" + model.VendorID + model.Title + ".png";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("bmp"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.VendorID + model.Title + ".bmp";
                        filePath = "../../Content/images/" + model.VendorID + model.Title + ".bmp";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("gif"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.VendorID + model.Title + ".gif";
                        filePath = "../../Content/images/" + model.VendorID + model.Title + ".gif";
                        model.Image.SaveAs(savePath);
                    }
                    else
                    {
                        filePath = "../../Content/images/defaultCatalogue.png";
                    }

                }
                else
                {
                    filePath = "../../Content/images/defaultCatalogue.png";
                }

                //Get catalogue matching current edit
                Catalogue cat = context.Catalogues.Where(
                    c => c.CATALOGUE_ID == model.ID).Single<Catalogue>();

                cat.CAT_TITLE = model.Title;
                cat.CAT_BANNER = filePath;
                cat.CAT_DESCRIPTION = model.Description;

                context.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Catalogue/Delete/5
        public ActionResult Delete(int id)
        {
             CatalogueViewModel model = context.Catalogues.Where(
                cat => cat.CATALOGUE_ID == id).Select(
                cat => new CatalogueViewModel()
                {
                    ID = cat.CATALOGUE_ID,
                    Title = cat.CAT_TITLE,
                    Description = cat.CAT_DESCRIPTION

                }).SingleOrDefault();
            return View(model);
        }

        // POST: Catalogue/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, CatalogueViewModel model)
        {
            try
            {
                Catalogue cat = context.Catalogues.Where(
                    c => c.CATALOGUE_ID == model.ID).Single<Catalogue>();

                context.Catalogues.DeleteOnSubmit(cat);
                context.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }
    }

    public class ItemController : Controller
    {
        private LINQ2AIPASPBDDataContext context;

        public ItemController()
        {
            context = new LINQ2AIPASPBDDataContext();
        }

        public ActionResult Index(int id)
        {
            Session["CatalogueID"] = id;
            //Creating list to store list of catalogues
            IList<ItemViewModel> listOfItems = new List<ItemViewModel>();

            //Creating query to retrieve all lists
            var query = from item in context.Items
                        where item.CATALOGUE_ID == id
                        select item;

            var items = query.ToList();

            foreach (var i in items)
            {
                listOfItems.Add(new ItemViewModel
                {
                    ID = i.ITEM_ID,
                    CatalogueID = i.CATALOGUE_ID,
                    Name = i.ITEM_NAME,
                    Type = i.ITEM_TYPE,
                    Price = int.Parse( i.ITEM_PRICE.ToString()),
                    Url = i.IMAGE_URL
                });
            }

            return View(listOfItems);

        }

        public PartialViewResult Browse(string objective)
        {
            var model = new object();
            
            model = loadListOfItems(objective);
            if (model == null)
            {
                return PartialView("Error");
            }
       
            return PartialView(model);
        }

        private object loadListOfItems(string objective)
        {
            IList<ItemViewModel> listOfItems = new List<ItemViewModel>();

            var query = from item in context.Items
                        where item.ITEM_TYPE.CompareTo(objective) == 0
                        select item;

            var items = query.ToList();

            foreach (var i in items)
            {
                listOfItems.Add(new ItemViewModel
                {
                    ID = i.ITEM_ID,
                    CatalogueID = i.CATALOGUE_ID,
                    Name = i.ITEM_NAME,
                    Type = i.ITEM_TYPE,
                    Price = int.Parse(i.ITEM_PRICE.ToString()),
                    Url = i.IMAGE_URL
                });
            }

            return listOfItems;
        }

        public ActionResult Details(int id)
        {
            ItemViewModel model = context.Items.Where(
                 item => item.ITEM_ID == id).Select(
                 item => new ItemViewModel()
                 {
                     ID = item.ITEM_ID,
                     CatalogueID = item.CATALOGUE_ID,
                     Name = item.ITEM_NAME,
                     Type = item.ITEM_TYPE,
                     Price = int.Parse(item.ITEM_PRICE.ToString()),
                     Url = item.IMAGE_URL
                 }).SingleOrDefault();

            return View(model);
        }

        public ActionResult Create()
        {
            ItemViewModel model = new ItemViewModel();

            model.Category = context.Vendors.Where(
                v => v.VENDOR_ID == int.Parse(Session["VendorID"].ToString())).Single().TYPE;

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(ItemViewModel model)
        {
            try
            {
                string savePath = null;
                string filePath = null;

                if (model.Image != null)
                {
                    if (model.Image.ContentType.Contains("jpeg") || model.Image.ContentType.Contains("jpg"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.CatalogueID + model.Name + ".jpeg";
                        filePath = "../../Content/images/" + model.CatalogueID + model.Name + ".jpeg";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("png"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.CatalogueID + model.Name + ".png";
                        filePath = "../../Content/images/" + model.CatalogueID + model.Name + ".png";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("bmp"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.CatalogueID + model.Name + ".bmp";
                        filePath = "../../Content/images/" + model.CatalogueID + model.Name + ".bmp";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("gif"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.CatalogueID + model.Name + ".gif";
                        filePath = "../../Content/images/" + model.CatalogueID + model.Name + ".gif";
                        model.Image.SaveAs(savePath);
                    }
                    else
                    {
                        filePath = "../../Content/images/defaultItem.png";
                    }

                }
                else
                {
                    filePath = "../../Content/images/defaultItem.png";
                }

                Item item = new Item()
                {
                    ITEM_NAME= model.Name,
                    CATALOGUE_ID = int.Parse(Session.Contents["CatalogueID"].ToString()),
                    ITEM_PRICE = (int) model.Price,
                    ITEM_TYPE = model.Type,
                    IMAGE_URL = model.Url
                };

                context.Items.InsertOnSubmit(item);
                context.SubmitChanges();

                return RedirectToAction("Index/" + Session.Contents["CatalogueID"].ToString());  
            }
            catch
            {
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            ItemViewModel model = context.Items.Where(
            item => item.ITEM_ID == id).Select(
            item => new ItemViewModel()
            {
                ID = item.ITEM_ID,
                CatalogueID = item.CATALOGUE_ID,
                Name = item.ITEM_NAME,
                Type = item.ITEM_TYPE,
                Price = int.Parse(item.ITEM_PRICE.ToString()),
                Url = item.IMAGE_URL,
                Category = context.Vendors.Where(
                v => v.VENDOR_ID == int.Parse(Session["VendorID"].ToString())).Single().TYPE
        }).Single();

            return View(model);
           
        }

        // POST: Item/Edit/5
        [HttpPost]
        public ActionResult Edit(ItemViewModel model)
        {
            try
            {
                string savePath = null;
                string filePath = null;

                if(model.Image != null)
                {
                    if (model.Image.ContentType.Contains("jpeg") || model.Image.ContentType.Contains("jpg"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.CatalogueID + model.Name + ".jpeg";
                        filePath = "../../Content/images/" + model.CatalogueID + model.Name + ".jpeg";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("png"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.CatalogueID + model.Name + ".png";
                        filePath = "../../Content/images/" + model.CatalogueID + model.Name + ".png";
                        model.Image.SaveAs(savePath);
                    }
                    else if (model.Image.ContentType.Contains("bmp"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.CatalogueID + model.Name + ".bmp";
                        filePath = "../../Content/images/" + model.CatalogueID + model.Name + ".bmp";
                        model.Image.SaveAs(savePath);
                    }
                    else if(model.Image.ContentType.Contains("gif"))
                    {
                        savePath = Server.MapPath("~/Content/images/") + model.CatalogueID + model.Name + ".gif";
                        filePath = "../../Content/images/" + model.CatalogueID + model.Name + ".gif";
                        model.Image.SaveAs(savePath);
                    }
                    else
                    {
                        filePath = "../../Content/images/defaultItem.png";
                    }

                }
                else
                {
                    filePath = "../../Content/images/defaultItem.png";
                }

                //Get Item matching current edit
                Item item = context.Items.Where(
                    i => i.ITEM_ID == model.ID).Single<Item>();

                item.ITEM_NAME = model.Name;
                item.CATALOGUE_ID = int.Parse(Session.Contents["CatalogueID"].ToString());
                item.ITEM_PRICE = (int)model.Price;
                item.ITEM_TYPE = model.Type;
                item.IMAGE_URL = filePath;

                context.SubmitChanges();

                return RedirectToAction("Index/" + Session.Contents["CatalogueID"].ToString());
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Item/Delete/5
        public ActionResult Delete(int id)
        {
            ItemViewModel model = context.Items.Where(
            item => item.ITEM_ID == id).Select(
            item => new ItemViewModel()
            {
                ID = item.ITEM_ID,
                CatalogueID = item.CATALOGUE_ID,
                Name = item.ITEM_NAME,
                Type = item.ITEM_TYPE,
                Price = int.Parse(item.ITEM_PRICE.ToString()),
                Url = item.IMAGE_URL
            }).SingleOrDefault();

            return View(model);
        }

        // POST: Item/Delete/5
        [HttpPost]
        public ActionResult Delete(ItemViewModel model)
        {
            try
            {
                Item item = context.Items.Where(
                                c => c.ITEM_ID == model.ID).Single<Item>();

                context.Items.DeleteOnSubmit(item);
                context.SubmitChanges();

                return RedirectToAction("Index/" + Session.Contents["CatalogueID"].ToString());

            }
            catch
            {
                return View(model);
            }
        }
    }

}
