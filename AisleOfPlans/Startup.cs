﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AisleOfPlans.Startup))]
namespace AisleOfPlans
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
