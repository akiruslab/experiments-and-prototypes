﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AisleOfPlans.View_Models
{
    public class PlanViewModel
    {
        public int Plan_ID { get; set; }
        public int Wedding_ID { get; set; }
        public int Priority_level { get; set; }
        public int PlanType_ID { get; set; }
        public string Plan_Name { get; set; }

        [Display(Name = "Plan Status")]
        public string Status { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime Deadline { get; set; }

        [Display(Name = "Completion")]
        public int Amount_Complete { get; set; }

        [Display(Name = "Estimated Cost")]
        public int Cost { get; set; }
    }

    public class PlanObjectiveViewModel
    {
        public int Objective_ID { get; set; }
        public int Plan_ID { get; set; }

        public string Name { get; set; }
        public string Status { get; set; }
        public string isComplete { get; set; }
    }

    public class ScrapbookViewModel
    {
        public int Scrapbook_ID { get; set; }
        public int Client_ID { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public string Pic_Location { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Upload Profile Picture")]
        public HttpPostedFileBase Image { get; set; }
    }

    public class BudgetViewModel
    {
        public int Budget_ID { get; set; }
        public int Wedding_id { get; set; }
        [Display(Name = "Available Budget")]
        public int Budget_Available { get; set; }
        [Display(Name = "Approximate Budget Used")]
        public int Budget_Used { get; set; }
    }

    public class GuestListViewModel
    {
        public int GuestList_ID { get; set; }
        public int Wedding_id { get; set; }
        [Display(Name = "Guests Expected")]
        public int Total_Guests { get; set; }
        [Display(Name = "Guests Attending")]
        public int Total_Attending { get; set; }
        [Display(Name = "Guests Pending")]
        public int Total_Pending { get; set; }
        [Display(Name = "Guests Not Attending")]
        public int Total_NonAttending { get; set; }

        public List<AisleOfPlans.Models.Guest> listOfGuests { get; set; }
    }

    public class GuestViewModle
    {
        /* Basic profile details*/
        public int Guest_ID { get; set; }
        public string GuestList_ID { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Lastname")]
        public string Lastname { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Contact number")]
        public string ContactNumber { get; set; }

        [Display(Name = "Reference number")]
        public string RefNumber { get; set; }

        [Display(Name = "How many people are you bringing?")]
        public string Additions { get; set; }
    }

    public class WeddingViewModel
    {
        public int Client_ID { get; set; }
        public int Wedding_ID { get; set; }

        [Display(Name = "Date of Ceremony")]
        [DataType(DataType.DateTime)]
        public DateTime WeddingDate { get; set; }

        [Display(Name = "Theme of Wedding")]
        public string Theme { get; set; }
        public string Status { get; set; }

        [Display(Name = "Street")]
        public string Street { get; set; }

        [Display(Name = "Suburb")]
        public string Suburb { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }

        [Display(Name = "Province/State")]
        public string Province { get; set; }

        [Display(Name = "Country")]
        public string Country { get; set; }

        public BudgetViewModel budget{get; set;}
        public AisleOfPlans.View_Models.ClientViewModel Client { get; set; }

        public AisleOfPlans.View_Models.GuestListViewModel GuestList { get; set; }
    }

    public class PlanTypeViewModel
    {
        public int PlanType_ID { get; set; }

        [Display(Name = "Type Name")]
        public string Name { get; set; }

        [Display(Name ="Type Description")]
        public string Description { get; set; }
    }
}