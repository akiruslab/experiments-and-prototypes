﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AisleOfPlans.Models;

namespace AisleOfPlans.View_Models
{
    public class ServiceRequestViewModel
    {
        public int Request_ID { get; set; }
        public int Item_ID { get; set; }
        public int WeddingID { get; set; }

        /////////////*Attire Request*//////////////
        [Display(Name = "Number of Items")]
        public int NumItems { get; set; }

        [Display(Name = "Height Measurement (m)")]
        public int HeightMeasurement { get; set; }

        [Display(Name = "Bust Measurement (cm)")]
        public int BustMeasurement { get; set; }

        [Display(Name = "Waist Measurement (cm)")]
        public int WaistMeasurement { get; set; }

        [Display(Name = "Hip Measurement (cm)")]
        public int HipMeasurement { get; set; }

        [Display(Name = "Shoe Size (UK)")]
        public int ShoeSize { get; set; }

        [Display(Name = "Date of fitting")]
        public DateTime DateOfFitting { get; set; }

        [Display(Name = "Other Details and Fashion Concerns")]
        public String FashionDetails { get; set; }

        /////////////*Catering Request*//////////////
        [Display(Name = "Number of Cakes")]
        public int NumCakes { get; set; }

        [Display(Name = "Number of Days Service is Required")]
        public int NumCateringDays { get; set; }

        [Display(Name = "Includes Alchoholic Refreshments")]
        public string isAlchoholic { get; set; }

        [Display(Name = "Other Details and Dietary Concerns")]
        public string DieteryDetails { get; set; }

        [Display(Name = "Date of Tasting")]
        public DateTime DateOfTasting { get; set; }

        /////////////*Vehicle  Request Creation*//////////////
        [Display(Name = "Number of Hours Vehicle is Required")]
        public int NumVehicleDays { get; set; }

        [Display(Name = "Number of Vehicles")]
        public int NumVehicles { get; set; }

        [Display(Name = "Other Details and Concerns")]
        public string VehicleDetails { get; set; }

        [Display(Name = "Date of Testing")]
        public DateTime DateOfTesting{ get; set; }

        /////////////*Entertainment Request Creation*//////////////
        [Display(Name = "Number of Hours Performance is Required")]
        public int NumPerformanceHours { get; set; }

        [Display(Name = "Other Details and Entertainment Preferences")]
        public string EntertainmentDetails { get; set; }

        ///////////*Media Request Details*////////////
        [Display(Name = "Number of Hours Service is Required")]
        public int NumServiceHours { get; set; }

        [Display(Name = "Other Details and Media Concerns")]
        public string MediaDetails { get; set; }

        ///////////*Venue Request Details*////////////
        [Display(Name = "Number of Days Venue is Required")]
        public int NumDays { get; set; }
       
        [Display(Name = "Require Use of Accommodation")]
        public string isAccommodationRequired { get; set; }

        [Display(Name = "Other Details and Venue Concerns")]
        public string VenueDetails { get; set; }

        [Display(Name = "Date of Viewing")]
        public DateTime DateOfViewing { get; set; }


        ///////////*Styling Request Details*////////////
        [Display(Name = "Number of People")]
        public int NumPeople { get; set; }
       
        [Display(Name = "Other Details and Styling Concerns")]
        public string StylingDetails { get; set; }

        ///////////*Art/decor Request Details*////////////
        [Display(Name = "Number of Packages Required")]
        public int NumPackages { get; set; }

        [Display(Name = "Other Details and Concerns")]
        public string ArtDetails { get; set; }

        ///////////*Request Details*////////////

        [Display(Name = "Requested Item")]
        public string ItemName { get; set; }

        [Display(Name = "Requested Item Type")]
        public string ItemType { get; set; }

        [Display(Name = "Requested Item Tags")]
        public string ItemTags { get; set; }

        [Display(Name = "Requested Details")]
        public string RequestDetails { get; set; }

        public string Image_URL { get; set; }

        [Display(Name = "Date of Request")]
        public DateTime Date_Requested { get; set; }

        [Display(Name = "Status of Request")]
        public string Status { get; set; }

        public Catalogue ItemCatalogue { get; set; }
        public Vendor ItemVendor { get; set; }

        public String proofLocation { get; set; }

        /////////////*Request Decline*//////////////
        [Display(Name = "Reason for Declining")]
        public string ReasonForDecline { get; set; }

        /////////////*Request Completion*//////////////
        [DataType(DataType.Upload)]
        [Display(Name = "Upload Proof of Payment")]
        public HttpPostedFileBase proof { get; set; }
    }

    public class PaymentProofViewModel
    {
        public int Proof_ID { get; set; }
        public int Request_ID { get; set; }

        public PaymentProof PDF { get; set; }
    }
}