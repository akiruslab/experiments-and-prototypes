﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AisleOfPlans.Models;
namespace AisleOfPlans.View_Models
{
    public class VendorReportViewModel
    {
        /*Request report details*/
        public int TotalSRequests { get; set; }
        public int TotalSRequestsComplete { get; set; }
    
        public int[][] SRequestsPerItem { get; set; }

        public int[][] SRequestsPerCat { get; set; }

        //TODO: region of requests


        /*Earning report details for dashboard*/
        public int TotalEstEarnings { get; set; }

        public int HighestItemEarnings { get; set; }

        public int HighestCatEarnings { get; set; }

        public int[][] EstEarningsPerItem { get; set; }

        public int[][] EstEarningsPerCat { get; set; }

        /*Popularity report details*/
        public Item MostEarningItem { get; set; }

        public Catalogue MostEarningCatalog { get; set; }

        //TODO: region of earnings

    }

    public class AdminReportViewModel
    {
        /*Site report details*/
        public int TotalRegistered { get; set; }
        public int TotalSetup { get; set; }
        public int TotalClients { get; set; }
        public int TotalVendors { get; set; }

        public int[][] RegPerTime { get; set; }

        /*Client report details*/
        public int TotalWeddings { get; set; }

        public int TotalWedPlanning { get; set; }

        public int TotalWedCanceled { get; set; }

        public int TotalWedComplete { get; set; }

        public int[][] TotalWedPerRegion { get; set; }

        public int[][] TotalSpentPerRegion { get; set; }

        public int[][] TotalSpentPerTime { get; set; }

        public int[][] TotalSpentPerPlan { get; set; }

        /*Vendor report details*/
        public int[][] TotalSpentPerType { get; set; }
        public String MostPopularTag { get; set; }
        public String MostPopularType { get; set; }
        public String MostRequetedItem { get; set; }

        public String MostPopularCatalog { get; set; }

        public String MostPopularVendor { get; set; }
    }
}