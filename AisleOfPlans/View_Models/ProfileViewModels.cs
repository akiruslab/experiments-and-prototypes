﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AisleOfPlans.Models;
using System.ComponentModel.DataAnnotations;


namespace AisleOfPlans.View_Models
{
    public class ClientViewModel
    {
        /* Basic profile details*/
        public int Client_ID { get; set; }
        public string User_ID { get; set; }

        [Display(Name = "Name of Partner")]
        public string Partner1_Name { get; set; }

        [Display(Name = "Name of One More Partner")]
        public string Partner2_Name { get; set; }

        [Required]
        [Display(Name = "New Lastname")]
        public string Lastname { get; set; }

        public string Pic_Location { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Upload Profile Picture")]
        public HttpPostedFileBase Image { get; set; }

        /*Location of client for delivery purposes*/
        public Wedding ClientWedding { get; set; }

        [Display(Name = "Street")]
        public string Street { get; set; }

        [Display(Name = "Suburb")]
        public string Suburb { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }

        [Display(Name = "Province")]
        public string Province { get; set; }

        [Display(Name = "Country")]
        public string Country { get; set; }

        [Display(Name = "Postal Code")]
        public string Code { get; set; }

        /*Planning Report details*/
        public int TotalDaysLeft { get; set; }
        public int OverallProgress { get; set; }

        public Budget budget { get; set; }

        public List<AisleOfPlans.View_Models.PlanViewModel> listOfPlans { get; set; }

        /*Guestlist Report details*/
        public AisleOfPlans.View_Models.GuestListViewModel GuestList { get; set; }

    }

    public class VendorViewModel
    {
        /*Basic vendor details*/
        public int Vendor_ID { get; set; }
        public string User_ID { get; set; }

        [Required]
        [Display(Name = "Name of your Business")]
        public string Business_Name { get; set; }

        [Required]
        [Display(Name = "Business Description")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Type of Service/Product you Provide")]
        public string Type { get; set; }

        [Display(Name = "Website Address(optional)")]
        public string Web_Address { get; set; }

        public string Logo_Location { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Upload your Logo")]
        public HttpPostedFileBase Image { get; set; }

        /*Vendor Location details*/
        [Display(Name = "Street")]
        public string Street { get; set; }

        [Display(Name = "Suburb")]
        public string Suburb { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }

        [Display(Name = "Province")]
        public string Province { get; set; }

        [Display(Name = "Country")]
        public string Country { get; set; }

        /*Vendor Banking details*/
        [Required]
        [Display(Name = "Bank")]
        public string Bank { get; set; }

        [Required]
        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }

        [Required]
        [Display(Name = "Branch Name")]
        public string BankingDetails { get; set; }

        [Required]
        [Display(Name = "Branch Number/Code")]
        public string BranchNumber { get; set; }

        /*Approximate Earning details*/
        public VendorReportViewModel approxEarnings { get; set; }

        /*Catalogue and item details*/
        public int NumCatalogues { get; set; }

        public int NumItems { get; set; }
    }

    public class UserViewModel
    {
        public int User_ID { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Phone Number")]
        public string Phone_Number { get; set; }

        public string Status { get; set; }
        public string Date_Registered { get; set; }
        public bool isSetup { get; set; }
    }
}