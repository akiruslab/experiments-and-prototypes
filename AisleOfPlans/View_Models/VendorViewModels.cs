﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AisleOfPlans.View_Models
{
    /*
     * 
     * */
    public class CatalogueViewModel
    {
        public int ID { get; set; }
        public int VendorID { get; set; }

        [Required]
        [Display(Name ="Catalogue Title")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Catalogue Description")]
        public string Description { get; set; }

        public string ImageUrl { get; set; }

        [DataType (DataType.Upload)]
        [Display(Name = "Upload Catalogue Banner")]
        public HttpPostedFileBase Image { get; set; }
    }

    public class ItemViewModel
    {
        public int ID { get; set; }
        public int CatalogueID { get; set; }
        public string Category { get; set; }
        public string Url { get; set; }

        [Required]
        [Display(Name = "Name of Item")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Type of Item")]
        public string Type { get; set; }

        [Required]
        [Display(Name = "Price of Item")]
        public int Price { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Uplaod Item Image")]
        public HttpPostedFileBase Image { get; set; }
    }

}