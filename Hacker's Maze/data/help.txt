			***Single Player Mission***
=========================================================================================================================================
You are a tasked with getting through a computer network which is displayed to you as a maze in order to help you hack the system without 
the experience of a pro. Your task is to gain valuable data hidden within before the security system blocks your point of entry forever. 
So make sure you grab the data and exit before you lose your way out. Remember that no network is the same so be prepared for anything. 

		(If the maze does not generate properly pause and restart)
========================================================================================================================================
			         ***Controls***
			 ARROW UP = Move player up
		   ARROW DOWN = Move player down
		   ARROW LEFT = Move player left
		  ARROW RIGHT = Move player right
			      ESC = Pause
			    Mouse = Menu navigation
========================================================================================================================================

