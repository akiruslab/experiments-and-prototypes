@echo off
cls
set path=%path%;C:\Program Files (x86)\Java\jdk1.7.0_79\bin
cd ..
set BIN=./bin
set DOCS=./docs
set LIB=./lib
set SRC=./src
echo *** Compiling ***
javac -sourcepath %SRC% -cp %BIN%;%LIB% -d %BIN% %SRC%/za/ac/uj/csc2b10/main/Main.java
IF %ERRORLEVEL% GTR 0 GOTO ERROR
echo *** Building JavaDoc ***
javadoc -sourcepath %SRC% -classpath %BIN%;%LIB% -use -author -d %DOCS%\JavaDocs -subpackages .
IF %ERRORLEVEL% GTR 0 GOTO ERROR
echo *** Running application ***
java -cp %BIN% za.ac.uj.csc2b10.main.Main 
IF %ERRORLEVEL% GTR 0 GOTO ERROR
GOTO END
:ERROR
echo !!! An error has occured !!!
echo Error number is %ERRORLEVEL%
:END
echo *** Completed ***
cd %DOCS%
pause
