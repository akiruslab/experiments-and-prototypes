package za.ac.uj.csc3a10.GUI;

import java.awt.DisplayMode;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Window;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;
/**
 * Not implemented for PX
 * @author Mazibila A.K.
 *
 */
public class ScreenManager
 {
private GraphicsDevice gpu;
	/**
	 * 
	 */
	public ScreenManager()
	{
		GraphicsEnvironment gE = GraphicsEnvironment.getLocalGraphicsEnvironment();
		gpu = gE.getDefaultScreenDevice();
	}
	
	/**
	 * Gets all compatible display modes
	 * @return 
	 */
	public DisplayMode[] getCompatibleDM()
	{
		return gpu.getDisplayModes();  
	}
	
	public DisplayMode findCompatibleDM(DisplayMode mode[])
	{
		DisplayMode compatibleMode[] = gpu.getDisplayModes();
		for (int c = 0; c < mode.length; c++)
		{
			for (int i = 0; i < compatibleMode.length; i++)
			{
				if (displayModeMatch(mode[c], compatibleMode[i]))
				{
					return compatibleMode[c];
				}
			}
		}
		return compatibleMode[0] = null;
	}
	
	private boolean displayModeMatch(DisplayMode displayMode,
			DisplayMode displayMode2)
	{
		
		return true;
	}

	public DisplayMode getCurrentDM()
	{
		return gpu.getDisplayMode();
	}
	
	public void setFullScreen(DisplayMode displayMode)
	{
		JFrame frame = new JFrame();
		frame.setUndecorated(true);
		frame.setIgnoreRepaint(true);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gpu.setFullScreenWindow(frame);
		
		if(displayMode != null && gpu.isDisplayChangeSupported())
		{
			try
			{
				gpu.setDisplayMode(displayMode);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			frame.createBufferStrategy(2);
		}
	}
	
	public Graphics2D getGraphics()
	{
		Window w = gpu.getFullScreenWindow();
		if (w != null)
		{
			BufferStrategy strat = w.getBufferStrategy();
			return (Graphics2D) strat.getDrawGraphics();
		}
		else
		{
			return null;
		}
	}
}
