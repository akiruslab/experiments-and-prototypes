package za.ac.uj.csc3a10.GUI;

import java.awt.image.BufferedImage;
/**
 * @author Mazibila A.K.
 *
 */
public class SpriteSheet 
{
	private BufferedImage img;
	/**
	 * 
	 * @param image
	 */
	public SpriteSheet(BufferedImage image)
	{
		img = image;
	}
	
	public BufferedImage getImage(int col, int row, int width, int height)
	{
		BufferedImage image  = img.getSubimage((col*45)-height, (row*width)-40, width, height);
		return image;
	}
}
