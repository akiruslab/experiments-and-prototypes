package za.ac.uj.csc3a10.GUI;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import za.ac.uj.csc3a10.Game.Game;
import za.ac.uj.csc3a10.Game.Screen;

/**
 * Main panel transfers rendering to its screen while
 * on the main frame.
 * @author Mazibila A.K.
 *
 */
public class MainPanel extends JPanel
{
	private static final long serialVersionUID = 2L;
	
	private Game game;
	private Screen screen;
	private ScreenPainter sP;
	
	public MainPanel(Game game)
	{	
		this.game = game;
		screen = null;
	}
	
	public MainPanel(Screen screen)
	{
		this.screen = screen;
	}

	public void update()
	{
		if(game.isInMenu())
		{
			repaint();
		}
		else if(game.isPlaying())
		{
			repaint();
		}
		else if(game.isPaused())
		{
			repaint();
		}
		else if(game.isGameOver())
		{
			repaint();
		}		
		else if(game.isGameWon())
		{
			repaint();
		}
	}
	
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		this.setBackground(Color.BLACK);
		if (screen != null)
		{
			sP = new ScreenPainter(g);
			screen.render(sP);
			g.setColor(Color.WHITE);
		}

	}
	/**
	 * 
	 * @return the screen
	 */
	public Screen getScreen() {
		return screen;
	}
	/**
	 * 
	 * @param screen the screen to set
	 */
	public void setScreen(Screen screen) {
		this.screen = screen;
	}
}
