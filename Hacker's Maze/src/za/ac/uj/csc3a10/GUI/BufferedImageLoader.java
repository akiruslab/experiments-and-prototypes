package za.ac.uj.csc3a10.GUI;

import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
/**
 * @author Mazibila A.K.
 *
 */
public class BufferedImageLoader
{
	private static BufferedImage img;
	
	public static BufferedImage loadImg(String path) throws IOException
	{
		img = ImageIO.read(new FileInputStream(path));
		return img;
	}
}
