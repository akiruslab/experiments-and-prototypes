
package za.ac.uj.csc3a10.GUI;

import java.awt.Image;
import java.util.ArrayList;

/**
 * @author Mazibila A.K.
 *
 */
public class Animation 
{
	private ArrayList<Frame> frame;
	private int frameIndex;
	private long movieTime;
	private long totalTime;
	
	/**
	 * 
	 */
	public Animation()
	{
		frame = new ArrayList<Frame>();
		movieTime = 0;
		totalTime =0;
		start();
	}
	
	public synchronized void addframe(Image img, long t)
	{
		totalTime+= t;
		frame.add(new Frame(img, totalTime));
	}
	
	public synchronized void start()
	{
		movieTime = 0;
		frameIndex = 0;
	}
	
	/**
	 * Changes frames 
	 * @param timePassed
	 */
	public synchronized void update(long timePassed)
	{
		if (frame.size() > 1)
		{
			movieTime += timePassed;
			if(movieTime >= totalTime)
			{//Loop done, restsart
				movieTime = 0;
				frameIndex = 0;
			}
			while(movieTime > getframe(frameIndex).endTime)
			{//The movie time is greater than the current frames time to end
				frameIndex++;
			}
		}
	}
	
	public synchronized Image getImage(int index)
	{
		if(frame.size() == 0)
		{
			return null;
		}
		else
		{
			return getframe(index).img;
		}
	}
	
	private Frame getframe(int x)
	{
		return (Frame) frame.get(x);
	}
	
	private class Frame
	{
		Image img;
		long endTime;
		
		public Frame(Image image, long endTime)
		{
			this.img = image;
			this.endTime = endTime;
		}
	}
}
