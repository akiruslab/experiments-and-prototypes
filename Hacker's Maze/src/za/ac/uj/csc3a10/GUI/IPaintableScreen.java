/**
 * 
 */
package za.ac.uj.csc3a10.GUI;

/**
 * @author Mazibila A.K.
 *
 */
public interface IPaintableScreen
{
	public void accept(ScreenPainter painter);
}
