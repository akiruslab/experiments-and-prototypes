/**
 * 
 */
package za.ac.uj.csc3a10.GUI;

import za.ac.uj.csc3a10.Game.GameOverScreen;
import za.ac.uj.csc3a10.Game.GameWonScreen;
import za.ac.uj.csc3a10.Game.MenuScreen;
import za.ac.uj.csc3a10.Game.MiniGameScreen;
import za.ac.uj.csc3a10.Game.PauseScreen;
import za.ac.uj.csc3a10.Game.PlayScreen;

/**
 * @author Mazibila A.K.
 *
 */
public  interface IScreenPainter
{
	/**
	 * 
	 * @param pauseS
	 */
	public abstract void paint(PauseScreen pauseS);
	/**
	 * 
	 * @param menuS
	 */
	public abstract void paint(MenuScreen menuS);
	/**
	 * 
	 * @param playS
	 */
	public abstract void paint(PlayScreen playS);
	
	/**
	 * 
	 * @param minGameS
	 */
	public abstract void paint(MiniGameScreen minGameS);
	/**
	 * 
	 * @param gameOverS
	 */
	public abstract void paint(GameOverScreen gameOverS);
	
	/**
	 * 
	 * @param gameWonS
	 */
	public abstract void paint(GameWonScreen gameWonS);
}
