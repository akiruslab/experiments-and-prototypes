/**
 * 
 */
package za.ac.uj.csc3a10.GUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.ImageIcon;

import za.ac.uj.csc3a10.Stage.Stage;
import za.ac.uj.csc3a10.Game.GameOverScreen;
import za.ac.uj.csc3a10.Game.GameWonScreen;
import za.ac.uj.csc3a10.Game.MenuScreen;
import za.ac.uj.csc3a10.Game.MiniGameScreen;
import za.ac.uj.csc3a10.Game.PauseScreen;
import za.ac.uj.csc3a10.Game.PlayScreen;

/**
 * @author Mazibila A.K.
 *
 */
public class ScreenPainter implements IScreenPainter
{
	private Graphics g;

	private int xPlayerSpritePos;
	private int yPlayerSpritePos;
	
	boolean goalFound;
	boolean backDoorFound;
	
	private static  int camX = 0;
	private static  int camY = 0;
	private static  int viewPortX = 16;
	private static  int viewPortY = 11;
	private static  int offSetX = 9;
	private static  int offSetY = 3;
	private static  int MIN_OFFSET_X = 0;
	private static  int MIN_OFFSET_Y = 0;
	
	Image tile;
	Image wall;
	Image goal;
	Image loss;
	Image victory;
	Image player;
	Image AI;
	Image backdoor;
	
	final int SCALE = 50;
	final int SPRITE_WIDTH = 40;
	final int SPRITE_LENGTH = 45;
	
	BufferedImage menuBackground;
	
	public ScreenPainter(Graphics g)
	{
		this.g = g;
		loadPics();
	}

	/* (non-Javadoc)
	 * @see za.ac.uj.csc2a10.GUI.IScreenPainter#paint(za.ac.uj.csc2a10.Game.PauseScreen)
	 */
	@Override
	public void paint(PauseScreen pauseS) 
	{
		g.setColor(Color.YELLOW);
		g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 24));
		g.drawString("Paused...", 300, 200);
	}

	/* (non-Javadoc)
	 * @see za.ac.uj.csc2a10.GUI.IScreenPainter#paint(za.ac.uj.csc2a10.Game.MenuScreen)
	 */
	@Override
	public void paint(MenuScreen menus) 
	{
		camX = 0;
		camY = 0;
		g.drawImage(menuBackground, 0,0,null);
	}

	/* (non-Javadoc)
	 * @see za.ac.uj.csc2a10.GUI.IScreenPainter#paint(za.ac.uj.csc2a10.Game.PlayScreen)
	 */
	@Override
	public void paint(PlayScreen playS)
	{	
		System.out.println("Player x-pos" + playS.getWorld().getPlayer().getxPos());
		System.out.println("Player y-pos" + playS.getWorld().getPlayer().getyPos());
		
			
		int[][] maze = playS.getWorld().getStage().getFloor();
		//Keep track of maze completion
		goalFound = (maze[0][0] ==Stage.GOAL_FOUND);

		g.setColor(Color.RED);
		

		switch(playS.getWorld().getPlayer().getDirection())
		{
			case Stage.NORTH:
				if(camY > MIN_OFFSET_Y && playS.getWorld().getPlayer().getyPos() < 6)
				{
					camY -=1;
					drawMap(maze, camX, camY);
				}
				else 
				{
					drawMap(maze, camX, camY);
				}
				
				//Draw maze from element from -1 element y
			break;
			case Stage.SOUTH:
				if(camY <= offSetY && playS.getWorld().getPlayer().getyPos()> 3)
				{
					camY += 1;
					drawMap(maze, camX, camY);	
				}
				else 
				{
					drawMap(maze, camX, camY);
				}
				
			break;
			case Stage.EAST:
				if(camX < offSetX && playS.getWorld().getPlayer().getxPos() > 6)
				{
					camX += 1;
					drawMap(maze, camX, camY);

				}
				else 
				{
					drawMap(maze, camX, camY);
					
				}

			break;
			case Stage.WEST:
				if(camX > MIN_OFFSET_X && playS.getWorld().getPlayer().getxPos() < 16)
				{
					camX -= 1;
					drawMap(maze, camX, camY);
				}
				else
				{
					drawMap(maze, camX, camY);					
				}

			break;
			default:
				drawMap(maze, camX, camY);
				//g.drawImage(player, (camX+1) *SCALE, (camY+1)*SCALE, null);				

				break;
		}
	}

	private void drawMap(int[][] maze, int camX, int camY)
	{
		for (int row = 0; row < viewPortY; row++)
		{
			for (int col = 0; col < viewPortX; col++)
			{
				switch (maze[row + camY][col + camX])
				{
				case Stage.WALL:
					g.drawImage(wall, col*SCALE, row*SCALE, null);
					System.out.print("#");
					break;
				case Stage.PATH:
					g.drawImage(tile, col*SCALE, row*SCALE, null);
					System.out.print("0");
					break;
				case Stage.PLAYER:
					g.drawImage(player, col*SCALE, row*SCALE, null);
					System.out.print("x");
					break;
				case Stage.AI:
					g.drawImage(AI, col*SCALE, row*SCALE, null);
					System.out.print("$");
					break;
				case Stage.BACKDOOR:
					g.drawImage(backdoor, col*SCALE, row*SCALE, null);
					System.out.print("+");
					break;
				case Stage.GOAL:
					g.drawImage(goal, col*SCALE, row*SCALE, null);
					System.out.print("^");
					break;
				default:
					break;
				}		
			}
			System.out.println("\n");
		}
	}
	private void loadPics()
	{
		//Loading stage pics
		 tile = new ImageIcon("res/sm-floor.png").getImage();
		 wall = new ImageIcon("res/sm-floor2.png").getImage();
		 goal = new ImageIcon("res/sm-treasure.png").getImage();
		 victory = new ImageIcon("res/congrats.png").getImage();
		 player = new ImageIcon("res/player.png").getImage();
		 AI = new ImageIcon("res/AI.png").getImage();
		 backdoor = new ImageIcon("res/backdoor1.png").getImage();
		 
		try
		{
			//playerSpriteSheet = BufferedImageLoader.loadImg("res/sm-player.gif");
			menuBackground = BufferedImageLoader.loadImg("res/menu.png");
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
	
		 
	/*	a = new Animation();
		
		a.addframe(standing, 250);
		a.addframe(step1, 250);
		a.addframe(step2, 250);*/
	}
	
	public int getxPos()
	{
		return xPlayerSpritePos;
	}
	
	public int getYpos()
	{
		return yPlayerSpritePos;
	}

	@Override
	public void paint(GameOverScreen gameOver) 
	{
		camX = 0;
		camY = 0;
		g.setColor(Color.RED);
		g.fillRect(100, 0, 600, 300);
		g.setColor(Color.BLACK);
		g.setFont(new Font(Font.SERIF, Font.BOLD, 24));
		g.drawString("Too slow the security system was able to close the ",180 , 150);
		g.drawString(" backdoor\n"
		+ "before you got the information.",180 , 200);

		
	}

	@Override
	public void paint(GameWonScreen gameWonS) {
		camX = 0;
		camY = 0;
		g.drawImage(victory, 90, 50, null);
	}

	@Override
	public void paint(MiniGameScreen minGameS) {
		// TODO Auto-generated method stub
		
	}
}
