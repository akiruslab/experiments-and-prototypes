/**
 * 
 */
package za.ac.uj.csc3a10.ScreenElement;

import za.ac.uj.csc3a10.Characters.AI;
import za.ac.uj.csc3a10.Stage.Stage;
import za.ac.uj.csc3a10.Characters.Player;

/**
 * @author Mazibila A.K.
 * 
 */
public class World extends ScreenElement
{
	private Player player;
	private AI cpu;
	private Stage stage;
	
	@Override
	public void init() 
	{
		setPlayer( new Player("User",0,"Black",0.5f, 1,1));
		setStage(new Stage("Treasure Maze", "large", "Low"));
		setCpu(new AI("Black Boots", 0.5f, 1, 1));
	}
	/**
	 * @return the player
	 */
	public Player getPlayer() {
		return player;
	}
	/**
	 * @param player the player to set
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}
	/**
	 * @return the stage
	 */
	public Stage getStage() {
		return stage;
	}
	/**
	 * @param stage the stage to set
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
	}
	
	/**
	 * @return the cpu which represents the AI in the game
	 */
	public AI getCpu() {
		return cpu;
	}
	
	/**
	 * @param cpu - AI that will be the player's opponent
	 */
	public void setCpu(AI cpu) {
		this.cpu = cpu;
	}

}
