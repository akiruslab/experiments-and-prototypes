/**
 * 
 */
package za.ac.uj.csc3a10.ScreenElement;

/**
 * @author Mazibila A.K.
 *
 */
public abstract class ScreenElement 
{
	private String name;
	
	public ScreenElement(){}
	
	public abstract  void init();
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
