package za.ac.uj.csc3a10.Game;

import za.ac.uj.csc3a10.ScreenElement.ScreenElement;
import za.ac.uj.csc3a10.GUI.IPaintableScreen;
import za.ac.uj.csc3a10.GUI.ScreenPainter;

/**
 * @author Mazibila A.K.
 *
 */
public abstract class Screen implements IPaintableScreen
{
	private ScreenElement ScreenElement;
	public abstract void init();
	public void render(ScreenPainter sPainter)
	{
		this.accept(sPainter);
	}

	/**
	 * @return the screenElement
	 */
	public ScreenElement getScreenElement() {
		return ScreenElement;
	}
	/**
	 * @param screenElement the screenElement to set
	 */
	public void setScreenElement(ScreenElement screenElement) {
		ScreenElement = screenElement;
	}
}
