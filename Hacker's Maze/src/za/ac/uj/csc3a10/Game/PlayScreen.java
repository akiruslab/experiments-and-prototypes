/**
 * 
 */
package za.ac.uj.csc3a10.Game;

import za.ac.uj.csc3a10.GUI.ScreenPainter;
import za.ac.uj.csc3a10.ScreenElement.World;

/**
 * @author Mazibila A.K.
 *
 */
public class PlayScreen extends Screen 
{
	World w;
	public PlayScreen()
	{
		w = new World();
		this.setScreenElement(w);
	}
	
	@Override
	public void accept(ScreenPainter painter) 
	{
		painter.paint(this);
	}

	@Override
	public void init() 
	{
		
	}

	public World getWorld()
	{
		return w;
	}
}
