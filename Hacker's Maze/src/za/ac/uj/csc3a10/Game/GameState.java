
package za.ac.uj.csc3a10.Game;

import javax.swing.JFrame;

import za.ac.uj.csc3a10.GUI.MainPanel;

/**
 * Abstract class that represents the game's state. While the game 
 * is in a specific state it will render and read in user input differently.
 * 
 * (Implementation of a State Design Pattern)
 * 
 * @author Mazibila A.K.
 *
 */
public abstract class GameState 
{
	private  boolean stateIsActive;
	public GameState(){	}
	/**
	 * Function to render the main panel in the current state
	 * of the game.
	 * 
	 * @param panel to be used to render.
	 */
	public abstract void render(MainPanel panel);
	public abstract void render(Game game, JFrame frame, MainPanel panel);
	/**
	 * Function that handles user input.
	 * 
	 * @param game that this state represents.
	 * @param frame frame that will listen for user input.
	 * @param panel to be used to render
	 */
	public abstract void processUserInput(Game game,JFrame frame, final MainPanel panel);
	/**
	 * @return the isActive
	 */
	public  boolean isActive() 
	{
		return stateIsActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public  void setActive(boolean isActive) 
	{
		stateIsActive = isActive;
	}
}
