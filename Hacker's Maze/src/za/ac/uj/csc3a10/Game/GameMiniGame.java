/**
 * 
 */
package za.ac.uj.csc3a10.Game;

import javax.swing.JFrame;

import za.ac.uj.csc3a10.GUI.MainPanel;

/**
 * @author Ken
 *
 */
public class GameMiniGame extends GameState {

	private static MiniGameScreen miniGameScreen;
	
	private static final GameMiniGame instanceOfState = new GameMiniGame();
	
	/**
	 * 
	 */
	public GameMiniGame()
	{
		miniGameScreen = new MiniGameScreen();
	}

	/* (non-Javadoc)
	 * @see za.ac.uj.csc2a10.Game.GameState#render(za.ac.uj.csc2a10.GUI.MainPanel)
	 */
	@Override
	public void render(MainPanel panel) 
	{
		panel.setScreen( miniGameScreen);
	}

	/* (non-Javadoc)
	 * @see za.ac.uj.csc2a10.Game.GameState#render(za.ac.uj.csc2a10.Game.Game, javax.swing.JFrame, za.ac.uj.csc2a10.GUI.MainPanel)
	 */
	@Override
	public void render(Game game, JFrame frame, MainPanel panel) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see za.ac.uj.csc2a10.Game.GameState#processUserInput(za.ac.uj.csc2a10.Game.Game, javax.swing.JFrame, za.ac.uj.csc2a10.GUI.MainPanel)
	 */
	@Override
	public void processUserInput(Game game, JFrame frame, MainPanel panel) {
		// TODO Auto-generated method stub

	}

	public static GameMiniGame getInstanceOfState() {
		
		return instanceOfState;
	}

}
