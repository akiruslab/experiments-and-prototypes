/**
 * 
 */
package za.ac.uj.csc3a10.Game;

import za.ac.uj.csc3a10.GUI.ScreenPainter;

/**
 * @author Mazibila A.K.
 *
 */
public class GameOverScreen extends Screen
{

	/* (non-Javadoc)
	 * @see za.ac.uj.csc2a10.GUI.IPaintableScreen#accept(za.ac.uj.csc2a10.GUI.ScreenPainter)
	 */
	@Override
	public void accept(ScreenPainter painter) {
		painter.paint(this);
	}

	/* (non-Javadoc)
	 * @see za.ac.uj.csc2a10.Game.Screen#init()
	 */
	@Override
	public void init() {
		// TODO Auto-generated method stub

	}

}
