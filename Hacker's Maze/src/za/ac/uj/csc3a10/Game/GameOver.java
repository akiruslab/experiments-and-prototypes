/**
 * 
 */
package za.ac.uj.csc3a10.Game;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import za.ac.uj.csc3a10.GUI.MainPanel;

/**
 * @author AKiRU
 *
 */
public class GameOver extends GameState
{
	private Screen gameOver;
	private Screen gameWon;
	private boolean hasEnded;
	private JButton btnRestart;
	private JButton btnMainMenu;
	private JButton btnExit;
	private static final GameOver instanceOfState = new GameOver();
	
	private GameOver()
	{
		gameOver = new GameOverScreen();
		gameWon = new GameWonScreen();
		hasEnded = false;
	}
	/* (non-Javadoc)
	 * @see za.ac.uj.csc2a10.Game.GameState#render(za.ac.uj.csc2a10.GUI.MainPanel)
	 */
	@Override
	public void render(MainPanel panel)
	{
		panel.setScreen(gameOver);
	}

	/* (non-Javadoc)
	 * @see za.ac.uj.csc2a10.Game.GameState#render(za.ac.uj.csc2a10.Game.Game, javax.swing.JFrame, za.ac.uj.csc2a10.GUI.MainPanel)
	 */
	@Override
	public void render(Game game, JFrame frame, MainPanel panel)
	{
		if(hasEnded == false)
		{
			createGameOverMenu(game, frame, panel);
			if(game.isTreasureFound() == true || game.isGameWon())
			{
				panel.setScreen(gameWon);
			}
			else if(game.isGameOver())
			{
				panel.setScreen(gameOver);
			}
			hasEnded = true;
		}
		else
		{
			btnRestart.setVisible(true);
			btnExit.setVisible(true);
			btnMainMenu.setVisible(true);
			if(game.isTreasureFound() == true)
			{
				panel.setScreen(gameWon);
			}
			else
			{
				panel.setScreen(gameOver);
			}			
			frame.requestFocus();
		}
	}

	private void createGameOverMenu(final Game game, final JFrame frame, MainPanel panel) {
		//Creating buttons new buttons
		btnRestart = new JButton("Restart");
		btnRestart.setBackground(Color.BLACK);
		btnRestart.setForeground(Color.WHITE);
		
		btnMainMenu = new JButton("Quit");
		btnMainMenu.setBackground(Color.BLACK);
		btnMainMenu.setForeground(Color.WHITE);
		
		btnExit = new JButton("Exit Game");
		btnExit.setBackground(Color.BLACK);
		btnExit.setForeground(Color.WHITE);
		
		btnRestart.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				btnMainMenu.setVisible(false);
				btnExit.setVisible(false);
				btnRestart.setVisible(false);
				
				game.setRestarted(true);
				game.setPlaying(true);
				
				game.update();
				frame.requestFocus();
				frame.repaint();
			}
		});

		btnMainMenu.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				btnExit.setVisible(false);
				btnRestart.setVisible(false);
				btnMainMenu.setVisible(false);
				game.setPaused(false);
				game.setInMenu(true);
				
				setActive(false);
				game.update();
				frame.requestFocus();	
				frame.repaint();
			}
		});
		
		btnExit.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				game.setRunning(false);
			}
		});
		
		panel.add(btnRestart);
		panel.add(btnMainMenu);
		panel.add(btnExit);
		panel.update();

	}
	/* (non-Javadoc)
	 * @see za.ac.uj.csc2a10.Game.GameState#processUserInput(za.ac.uj.csc2a10.Game.Game, javax.swing.JFrame, za.ac.uj.csc2a10.GUI.MainPanel)
	 */
	@Override
	public void processUserInput(Game game, JFrame frame, MainPanel panel) {

	}
	/**
	 * @return the instanceofstate
	 */
	public static GameOver getInstanceofstate() {
		return instanceOfState;
	}

}
