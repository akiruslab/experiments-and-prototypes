/**
 * 
 */
package za.ac.uj.csc3a10.Game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import za.ac.uj.csc3a10.Characters.AI;
import za.ac.uj.csc3a10.Stage.Stage;
import za.ac.uj.csc3a10.Util.MapVertex;
import za.ac.uj.csc3a10.Util.PathFinder;
import za.ac.uj.csc3a10.Util.Position;
import za.ac.uj.csc3a10.Characters.Player;
import za.ac.uj.csc3a10.GUI.MainPanel;

/**
 * State in which player is playing the game.
 * 
 * @author Mazibila A.K.
 *
 */
public class GamePlaying  extends GameState
{
	private static PlayScreen playScreen;
	private PathFinder backDoorFinder;
	private List<MapVertex<Position,Integer>> path;
	private int index;
	private Position nextPoint;
	private static int[][] stageArea;
	
	private boolean firstPlay;

	private static final GamePlaying instanceOfState = new GamePlaying();
	
	private GamePlaying()
	{
		firstPlay = true;
		path = new ArrayList<MapVertex<Position,Integer>>();
		index = 0;
		nextPoint = null;
		initWorld();
	}
	private void initWorld()
	{
		playScreen = new PlayScreen();
		playScreen.getWorld().init();
		stageArea = playScreen.getWorld().getStage().getFloor();
		this.setActive(true);
		initPathFinders();
		////////////////////////////////////////////////Path finding code stuck in infinite loop/////////////////////////////////////
		/*path = backDoorFinder.findPath();
		if(path == null)
		{
			path = getDefaultPath();
		}*/
	}

	private void initPathFinders() {
		backDoorFinder = new PathFinder(
				playScreen.getWorld().getStage().getGraph(), 
				playScreen.getWorld().getStage().getGraphStart(), 
				playScreen.getWorld().getStage().getEnd());
	}
	/**
	 * 
	 */
	public void restart()
	{
		initWorld();
	}
	
	@Override
	public void render(MainPanel panel) 
	{
		panel.setScreen(playScreen);
	}
	
	/**
	 * @return the instanceOfState
	 */
	public static GamePlaying getInstanceOfState()
	{
		return instanceOfState;
	}
	
	@Override
	public void processUserInput(final Game game,final JFrame frame, final MainPanel panel) 
	{
		if(firstPlay == true)
		{
			firstPlay = false;
			
			System.out.println("Setting the frame to listen");
			
			frame.addKeyListener(( new KeyListener()
			{
				@Override
				public void keyTyped(KeyEvent e) 
				{

				}

				@Override
				public void keyPressed(KeyEvent e) 
				{
					/*
					 * Following if statements control the player 
					 * while this state is currently active.
					 */
					if (e.getKeyCode() == KeyEvent.VK_DOWN)
					{
						walkingLoop(playScreen.getWorld().getPlayer(), Stage.SOUTH,game,frame);
						panel.update();

					}
					if (e.getKeyCode() == KeyEvent.VK_UP)
					{
						walkingLoop(playScreen.getWorld().getPlayer(), Stage.NORTH,game, frame);
						panel.update();

					}
					if (e.getKeyCode() == KeyEvent.VK_LEFT)
					{
						walkingLoop(playScreen.getWorld().getPlayer(), Stage.WEST, game, frame);	
						panel.update();

					}
					if (e.getKeyCode() == KeyEvent.VK_RIGHT)
					{
						walkingLoop(playScreen.getWorld().getPlayer(), Stage.EAST, game, frame);
						panel.update();

					}
					/*
					 * The following if statements control the input from the
					 * keyboard to either initiate the pause state.
					 */
					if(e.getKeyCode() == KeyEvent.VK_ESCAPE && game.isPlaying()==true)
					{
						game.setPaused(true);
						game.setPlaying(false);
						game.update();
						frame.requestFocus();
						frame.repaint();
					}
				}
	
				@Override
				public void keyReleased(KeyEvent e) 
				{
				
					
				}
			})); /*end of add key listener*/
					
				frame.validate();
			}
	}

	private void walkingLoop(Player p, int direction, Game game, final JFrame frame ) 
	{	
		if(direction == Stage.NORTH)
		{
			System.out.println("In front of player: " + stageArea[p.getyPos()-1][p.getxPos()]);
			if(stageArea[p.getyPos()-1][p.getxPos()] == Stage.PATH ||
					stageArea[p.getyPos()-1][p.getxPos()] == Stage.AI)
			{
				if(stageArea[p.getyPos()-1][p.getxPos()] == Stage.AI)
				{
					stageArea[p.getyPos()][p.getxPos()] = Stage.AI;
					stageArea[p.getyPos()-1][p.getxPos()] = Stage.PLAYER;
				}
				stageArea[p.getyPos()][p.getxPos()] = Stage.PATH;
				stageArea[p.getyPos()-1][p.getxPos()] = Stage.PLAYER;
	
				p.moveUP();
				System.out.println("Moved up");
	
			}
			else if (stageArea[p.getyPos()-1][p.getxPos()] == Stage.GOAL)
			{
				stageArea[p.getyPos()][p.getxPos()] = Stage.PATH;
				stageArea[p.getyPos()][p.getxPos()-1] = Stage.PLAYER;
	
				p.moveUP();
				
				//Changing state
				changeToGameWon(game, frame);
				
				System.out.println("Moved up and Found the Treasure");
	
			}
		}
		else if(direction == Stage.SOUTH)
		{
			if(stageArea[p.getyPos()+1][p.getxPos()] == Stage.PATH ||
					stageArea[p.getyPos()+1][p.getxPos()] == Stage.AI)
			{
				if(stageArea[p.getyPos()+1][p.getxPos()] == Stage.AI)
				{
					stageArea[p.getyPos()][p.getxPos()] = Stage.AI;
					stageArea[p.getyPos()+1][p.getxPos()] = Stage.PLAYER;
				}
				else
				{
					stageArea[p.getyPos()][p.getxPos()] = Stage.PATH;
					stageArea[p.getyPos()+1][p.getxPos()] = Stage.PLAYER;
				}

				p.moveDown();
				System.out.println("Moved down ");
	
			}
			else if (stageArea[p.getyPos()+1][p.getxPos()] == Stage.GOAL)
			{
				stageArea[p.getyPos()][p.getxPos()] = Stage.PATH;
				stageArea[p.getyPos()+1][p.getxPos()] = Stage.PLAYER;
	
				p.moveDown();
				
				//Changing state
				changeToGameWon(game, frame);
				System.out.println("Moved down and Found the Treasure");
	
			}
		}
		else if(direction == Stage.WEST)
		{
			if(stageArea[p.getyPos()][p.getxPos()-1] == Stage.PATH ||
					stageArea[p.getyPos()][p.getxPos()-1] == Stage.AI)
			{
				if(stageArea[p.getyPos()][p.getxPos()-1] == Stage.AI)
				{
					stageArea[p.getyPos()][p.getxPos()] = Stage.AI;
					stageArea[p.getyPos()][p.getxPos()-1] = Stage.PLAYER;
				}
				else
				{
					stageArea[p.getyPos()][p.getxPos()] = Stage.PATH;
					stageArea[p.getyPos()][p.getxPos()-1] = Stage.PLAYER;
				}

				p.moveLeft();
				
				System.out.println("Moved left");
	
			}
			else if (stageArea[p.getyPos()][p.getxPos() - 1] == Stage.GOAL)
			{
				stageArea[p.getyPos()][p.getxPos()] = Stage.PATH;
				stageArea[p.getyPos()][p.getxPos()-1] = Stage.PLAYER;
				p.moveLeft();
				
				//Changing state
				changeToGameWon(game, frame);
				System.out.println("Moved left and Found the Treasure");
			}
		}
		else if(direction == Stage.EAST)
		{
			if(stageArea[p.getyPos()][p.getxPos() + 1] == Stage.PATH ||
					stageArea[p.getyPos()][p.getxPos() + 1] == Stage.AI)
			{
				if(stageArea[p.getyPos()][p.getxPos() + 1] == Stage.AI)
				{
					stageArea[p.getyPos()][p.getxPos()] = Stage.AI;
					stageArea[p.getyPos()][p.getxPos() +1] = Stage.PLAYER;
				}
				else
				{
					stageArea[p.getyPos()][p.getxPos()] = Stage.PATH;
					stageArea[p.getyPos()][p.getxPos() +1] = Stage.PLAYER;
				}

				p.moveRight();
				System.out.println("Moved right");
	
			}
			else if (stageArea[p.getyPos()][p.getxPos() +1] == Stage.GOAL)
			{
				stageArea[p.getyPos()][p.getxPos()] = Stage.PATH;
				stageArea[p.getyPos()][p.getxPos()+1] = Stage.PLAYER;
	
				p.moveRight();
				
				//Changing state
				changeToGameWon(game, frame);

				System.out.println("Moved right and Found the Treasure");
	
			}
		}
	}	
	
	private void AIWalkingLoop(AI cpu, Game game, final JFrame frame)
	{
		nextPoint = path.get(index).getElement();
		if(game.isBackdoorFound()!= true)
		{
			cpu.setxPos(nextPoint.getxPos());
			cpu.setyPos(nextPoint.getyPos());
			
			if (stageArea[cpu.getyPos()-1][cpu.getxPos()] == Stage.BACKDOOR)
			{
				game.setBackdoorFound(true);
				index--;
			}
			else
				index++;
		}
		else
		{
			cpu.setxPos(nextPoint.getxPos());
			cpu.setyPos(nextPoint.getyPos());
			if (stageArea[cpu.getyPos()][cpu.getxPos()-1] == Stage.GOAL ||
					stageArea[cpu.getyPos()][cpu.getxPos()+1] == Stage.GOAL ||
					stageArea[cpu.getyPos()+1][cpu.getxPos()] == Stage.GOAL
					||stageArea[cpu.getyPos()-1][cpu.getxPos()] == Stage.GOAL)
			{
				changeToGameOver(game, frame);
			}
	
			index--;
		}
	}
	
	@Override
	public void render(Game game,JFrame frame, MainPanel panel) 
	{
		//AIWalkingLoop(playScreen.getWorld().getCpu(), game, frame);
		panel.setScreen(playScreen);
	}
	
	private void changeToGameWon(Game game, JFrame frame)
	{
		game.setTreasureFound(true);
		game.setGameOver(true);
		game.setPlaying(false);
		game.update();
		frame.requestFocus();
		frame.repaint();
	}
	private void changeToGameOver(Game game, JFrame frame)
	{
		game.setTreasureFound(false);
		game.setGameOver(true);
		game.setPlaying(false);
		game.update();
		frame.requestFocus();
		frame.repaint();
	}
	
	/**
	 * 
	 * @return
	 */
	public PlayScreen getPlayScreen()
	{
		return playScreen;
	}

	private List<MapVertex<Position, Integer>> getDefaultPath() {
		List<MapVertex<Position,Integer>> dPath = new ArrayList<>();
		
		return dPath;
	}
}
