/**
 * 
 */
package za.ac.uj.csc3a10.Game;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import za.ac.uj.csc3a10.GUI.MainPanel;

/**
 * @author Mazibila A.K.
 *
 */
public class GameMenu extends GameState
{
	private Screen mainMenuScreen;
	private boolean hasRendered;
	private static final GameMenu instanceOfState = new GameMenu();
	
	private JMenuBar mBar ;
	private JButton btnPlay;
	private JButton btnViewStats;
	private JButton btnExit;
	
	private GameMenu()
	{
		hasRendered = false;
		mainMenuScreen = new MenuScreen();
	}

	public void render(final Game game, final JFrame frame, final MainPanel panel)
	{
		if(hasRendered == false)
		{
			createMenu(game, frame, panel);
			hasRendered = true;
		}
		else
		{
			game.setRestarted(true);
			mBar.setVisible(true);
			btnPlay.setVisible(true);
			btnExit.setVisible(true);
			btnViewStats.setVisible(true);
			panel.setScreen(mainMenuScreen);	
		}
	}

	private void createMenu(final Game game, final JFrame frame, MainPanel panel) {
		//Creating new menu item
		JMenuItem help = new JMenuItem ("Mission and Controls");
		
		addHelpActionListener(help);
		
		//Adding menu items to a new menu
		JMenu helpMenu = new JMenu("Help");
		helpMenu.setForeground(Color.WHITE);
		helpMenu.add(help);
		
		//Adding menu to menu bar
		mBar = new JMenuBar();
		mBar.setBackground(Color.BLACK);
		mBar.setForeground(Color.WHITE);
		mBar.add(helpMenu);
		
		frame.setJMenuBar(mBar);

		//Creating buttons new buttons
		btnPlay = new JButton("Play");
		btnPlay.setSize(300, 50);
		btnPlay.setBackground(Color.BLACK);
		btnPlay.setForeground(Color.WHITE);
		
		btnViewStats = new JButton("ViewStats");
		btnViewStats.setSize(200, 50);
		btnViewStats.setBackground(Color.BLACK);
		btnViewStats.setForeground(Color.WHITE);
		
		btnExit = new JButton("Exit");
		btnExit.setSize(300, 50);
		btnExit.setBackground(Color.BLACK);
		btnExit.setForeground(Color.WHITE);
		
		btnPlay.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				mBar.setVisible(false);
				btnExit.setVisible(false);
				btnViewStats.setVisible(false);
				btnPlay.setVisible(false);
				game.setPlaying(true);

				game.update();
				frame.requestFocus();
			}
		});


		btnViewStats.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				viewStats();
			}
		});
		
		btnExit.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				game.setRunning(false);
			}
		});
	
		GridBagLayout layout = new GridBagLayout();
		panel.setLayout(layout);
		
		panel.add(btnPlay);
		panel.add(btnViewStats);
		panel.add(btnExit);
		frame.validate();
		
		panel.setScreen(mainMenuScreen);
	}

	protected void viewStats() 
	{
		final JFrame statsFrame = new JFrame();
		statsFrame.setSize(800, 550);
		statsFrame.setUndecorated(true);
		statsFrame.setLocationRelativeTo(null);
		statsFrame.setFocusable(true);

		JPanel statsPanel = new JPanel(new BorderLayout());
		statsPanel.setForeground(Color.BLACK);
		
		JTextArea txtStats = new JTextArea();
		txtStats.setBackground(Color.BLACK);
		txtStats.setForeground(Color.GREEN);
		statsPanel.add(txtStats, BorderLayout.NORTH);
		loadStats(txtStats);
		
		JButton closeButton = new JButton("Close");
		closeButton.setBackground(Color.BLACK);
		closeButton.setForeground(Color.WHITE);
		closeButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				statsFrame.dispose();
			}
		});
		statsPanel.add(closeButton, BorderLayout.CENTER);
		
		statsFrame.add(statsPanel);
		
		statsFrame.setVisible(true);
		statsFrame.requestFocus();		
	}

	/**
	 * @return the instanceOfState
	 */
	public static GameMenu getInstanceOfState() {
		return instanceOfState;
	}

	@Override
	public void processUserInput(Game game, JFrame frame,  MainPanel panel) 
	{
	}

	
	private void addHelpActionListener(JMenuItem help)
	{
		help.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				final JFrame helpFrame = new JFrame();
				helpFrame.setSize(800, 550);
				helpFrame.setUndecorated(true);
				helpFrame.setLocationRelativeTo(null);
				helpFrame.setFocusable(true);
	
				JPanel helpPanel = new JPanel(new BorderLayout());
				helpPanel.setForeground(Color.BLACK);
				
				JTextArea txtHelp = new JTextArea(5,3);
				txtHelp.setBackground(Color.BLACK);
				txtHelp.setForeground(Color.GREEN);
				helpPanel.add(txtHelp, BorderLayout.NORTH);
				loadHelp(txtHelp);
				
				JButton closeButton = new JButton("Close");
				closeButton.setBackground(Color.BLACK);
				closeButton.setForeground(Color.WHITE);
				closeButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e)
					{
						helpFrame.dispose();
					}
				});
				helpPanel.add(closeButton, BorderLayout.CENTER);
				
				helpFrame.add(helpPanel);
				
				helpFrame.setVisible(true);
				helpFrame.requestFocus();				
			}
		});

	}
	
	/*
	 * Reading in help file 
	 */
	private void loadHelp(JTextArea txtHelp )
	{
		File help = new File("data/help.txt");
		String helpText = "";
		Scanner scanFile = null;
		try {
			scanFile = new Scanner(help);
			while(scanFile.hasNext())
			{
				helpText +=  "\n" + scanFile.nextLine() +"\r\n";
			}
			scanFile.close();
		} catch (FileNotFoundException e) {
	
			e.printStackTrace();
		}
		finally
		{
			if (scanFile != null)
				scanFile.close();
		}
		txtHelp.setText(helpText);
	}
	
	/*
	 * Reading in stats file 
	 */
	private void loadStats(JTextArea txtStats )
	{
		File stats = new File("data/stats.txt");
		String statsText = "";
		Scanner scanFile = null;
		try {
			scanFile = new Scanner(stats);
			while(scanFile.hasNext())
			{
				statsText +=  "\n" + scanFile.nextLine() +"\r\n";
			}
			scanFile.close();
		} catch (FileNotFoundException e) {
	
			e.printStackTrace();
		}
		finally
		{
			if (scanFile != null)
				scanFile.close();
		}
		txtStats.setText(statsText);
	}

	
	@Override
	public void render(MainPanel panel) {
		
	}
}
