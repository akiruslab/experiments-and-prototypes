/**
 * 
 */
package za.ac.uj.csc3a10.Game;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import za.ac.uj.csc3a10.GUI.MainPanel;

/**
 * @author Mazibila A.K.
 *
 */
public class GamePaused  extends GameState
{
	private Screen pauseScreen;
	private boolean hasPaused;
	private JButton btnRestart;

	private JButton btnPlay;
	private JButton btnQuit;
	private JButton btnExit;
	private static final GamePaused instanceOfState = new GamePaused();
		
	private GamePaused()
	{
		pauseScreen = new PauseScreen();
		hasPaused = false;
	}
	@Override
	public void render(MainPanel panel) 
	{
		panel.setScreen(pauseScreen);	
	}
	/**
	 * @return the instanceOfState
	 */
	public static GamePaused getInstanceOfState() {
		return instanceOfState;
	}
	
	private void createPauseMenu(final Game game,final JFrame frame, MainPanel panel)
	{
		//Creating buttons new buttons
		btnPlay = new JButton("Resume");
		btnPlay.setBackground(Color.BLACK);
		btnPlay.setForeground(Color.WHITE);
		
		btnQuit = new JButton("Quit");
		btnQuit.setBackground(Color.BLACK);
		btnQuit.setForeground(Color.WHITE);
		
		btnExit = new JButton("Exit Game");
		btnExit.setBackground(Color.BLACK);
		btnExit.setForeground(Color.WHITE);
		
		btnPlay.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				btnRestart.setVisible(false);

				btnQuit.setVisible(false);
				btnExit.setVisible(false);
				btnPlay.setVisible(false);
				game.setPlaying(true);
				
				game.update();
				frame.requestFocus();
				frame.repaint();
			}
		});
		btnRestart = new JButton("Restart");
		btnRestart.setBackground(Color.BLACK);
		btnRestart.setForeground(Color.WHITE);
		btnRestart.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				btnPlay.setVisible(false);
				btnQuit.setVisible(false);
				btnExit.setVisible(false);
				btnRestart.setVisible(false);
				
				game.setRestarted(true);
				game.setPlaying(true);
				
				game.update();
				frame.requestFocus();
				frame.repaint();
			}
		});
		btnQuit.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				btnRestart.setVisible(false);

				btnExit.setVisible(false);
				btnPlay.setVisible(false);
				btnQuit.setVisible(false);
				game.setPaused(false);
				game.setInMenu(true);
				
				game.update();
				frame.requestFocus();			
			}
		});
		
		btnExit.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				game.setRunning(false);
			}
		});
		panel.add(btnRestart);

		panel.add(btnPlay);
		panel.add(btnQuit);
		panel.add(btnExit);
		
		panel.setScreen(pauseScreen);	
	}

	@Override
	public void render(Game game, JFrame frame, MainPanel panel) 
	{
		if(hasPaused == false)
		{
			createPauseMenu(game, frame, panel);
			hasPaused = true;
		}
		else
		{
			btnRestart.setVisible(true);
			btnPlay.setVisible(true);
			btnExit.setVisible(true);
			btnQuit.setVisible(true);
			panel.setScreen(pauseScreen);	
		}
	}
	
	@Override
	public void processUserInput(Game game, JFrame frame, MainPanel panel) 
	{

	}
}
