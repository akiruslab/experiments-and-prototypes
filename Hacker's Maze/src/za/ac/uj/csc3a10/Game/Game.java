package za.ac.uj.csc3a10.Game;

import javax.swing.JFrame;

import za.ac.uj.csc3a10.GUI.MainPanel;
import za.ac.uj.csc3a10.Util.GamingArrayList;
/**
 * @author Mazibila A.K.
 *
 */  
public class Game implements Runnable
{
		Thread t1;
		public static final String gameName = "Treasure Maze";
		private GamingArrayList<GameState> gs;

		private GameState currentState;
		private  boolean isPaused = false;
		private  boolean isInMenu = false;
		private boolean isInMiniGame = false;
		private boolean isPlaying = false;
		private boolean isGameOver = false;
		private boolean isGameWon = false;
		private boolean isMultiplayer = false;
		private boolean treasureFound = false;
		private boolean isBackdoorFound = false;
		private boolean isRestarted = false;
		//private boolean playerWon = false;
		//private boolean playerLost = false;
		private boolean isRunning;

		private JFrame mainFrame;
		private MainPanel mp;
		
		/**
		 * Constructor that allows the game's frame to be given a title,
		 * width and height before being initialized.
		 * 
		 * @param title of frame
		 * @param width of window
		 * @param height of window
		 */
		public Game(String title, int width, int height) 
		{
			isRunning = true;
			//Creating frame of the game
			mainFrame = new JFrame();
			mainFrame.setTitle(title);
			mainFrame.setSize(width, height);
			//mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
			mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			//mainFrame.setUndecorated(true);
			mainFrame.setResizable(false);
			mainFrame.setLocationRelativeTo(null);
			
			//Creating a new list of game states
			gs = new GamingArrayList<GameState> (5);
			gs.add(0,GameMenu.getInstanceOfState());
			gs.add(1,GamePlaying.getInstanceOfState());
			gs.add(2,GameMiniGame.getInstanceOfState());
			gs.add(3,GamePaused.getInstanceOfState());
			gs.add(4, GameOver.getInstanceofstate());
			
			//Setting the initial state
			currentState =gs.get(0);
			currentState.setActive(true);
			isInMenu = true;
			processUserInput();
		}
		
		/**
		 * Initializes the  the games panel and sets the
		 * games frame to visible before rendering the
		 * game.
		 */
		public void init()
		{
			//Creating main panel
			mp = new MainPanel(this);
			mainFrame.add(mp);//Adding main panel to frame
			mainFrame.setVisible(true);//Setting frame to visible
			
			render();//Rendering the game
		}
		/**
		 * Starts the games thread.
		 */
		public void start()
		{
			//Starting the games thread
			t1 = new Thread(this);
			t1.start();
		}
		/**
		 * Pauses the games thread
		 */
		public void pause()
		{
			try {
				t1.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		/**
		 * Renders the game by telling its state to render
		 * the specific graphics related to it.
		 */
		public void render()
		{
			if(isInMenu)
			{
				currentState.render(this,mainFrame, mp );
			}
			else if(isPlaying)
			{
				currentState.render(this,mainFrame, mp );
			}
			else if(isInMiniGame)
			{
				currentState.render(this,mainFrame, mp );
			}
			else if(isPaused)
			{
				currentState.render(this,mainFrame, mp );
			}
			else if(isGameOver)
			{
				currentState.render(this,mainFrame, mp );
			}
		}
		
		/**
		 * Function that updates the game's display
		 * panel.
		 */
		public void update()
		{
			render();
		}
		
		/**
		 * Function to exit game completely.
		 */
		public void destroy()
		{
			mainFrame.dispose();
			System.exit(0);
		}

		@Override
		public void run() 
		{
			while (isRunning() == true)
			{
				try 
				{
					if(isPlaying)
					render();

					Thread.sleep(20);
				}
				catch (InterruptedException ex)
				{
					
				}
			}
			
			destroy();
		}
		/**
		 * Function to make game to listen for user input while
		 * in its current states.
		 */
		public void processUserInput()
		{
			if(isInMenu)
			{
				currentState.processUserInput(this,mainFrame, mp);
			}
			else if(isPlaying)
			{
				currentState.processUserInput(this,mainFrame, mp);
			}
			else if(isInMiniGame)
			{
				currentState.processUserInput(this,mainFrame, mp);
			}
			else if(isPaused)
			{
				currentState.processUserInput(this,mainFrame, mp);
			}
			else if (isGameOver)
			{
				currentState.processUserInput(this,mainFrame, mp);		
			}
		}
		
		/**
		 * @return the isPaused
		 */
		public  boolean isPaused() {
			return isPaused;
		}

		/**
		 * @param isPaused the isPaused to set
		 */
		public  void setPaused(boolean paused) {
			this.isPaused = paused;
			
			if (paused == true)
			{
				//Since game is in paused state
				changeState( gs.get(3));
				
				//Making sure only paused is true
				isPlaying = (false);
				isInMenu = (false);
				isGameOver = false;
				isInMiniGame = false;

				currentState.setActive(true);
				gs.get(0).setActive(false);
				gs.get(2).setActive(false);
				gs.get(1).setActive(false);
				gs.get(4).setActive(false);
			}
			
			processUserInput();
			render();
		}

		/**
		 * @return the isInMenu
		 */
		public  boolean isInMenu() {
			return isInMenu;
		}

		/**
		 * @param isInMenu the isInMenu to set
		 */
		public  void setInMenu(boolean isInMenu) {
			this.isInMenu = isInMenu;
			
			if(isInMenu == true){
				isPaused = (false);
				isGameOver = false;
				isPlaying = (false);
				isInMiniGame = false;

				changeState( gs.get(0));
				currentState.setActive(true);
				
				gs.get(1).setActive(false);
				gs.get(3).setActive(false);
				gs.get(2).setActive(false);
				gs.get(4).setActive(false);
			}


			processUserInput();
			render();
		}

		/**
		 * @return the isPlaying
		 */
		public boolean isPlaying() {
			return isPlaying;
		}

		/**
		 * @param isPlaying the isPlaying to set
		 */
		public void setPlaying(boolean isPlaying) 
		{
			this.isPlaying = isPlaying;
			GamePlaying gPlaying = null;
			
		
			if(isRestarted == true)
			{
				gPlaying = (GamePlaying) gs.get(1);
				gPlaying.restart();
				gs.set(1, gPlaying);
			}
			isPaused = false;
			isInMenu = false;
			isGameOver = false;
			isInMiniGame = false;
			
			changeState( gs.get(1));
			currentState.setActive(true);
			
			gs.get(0).setActive(false);
			gs.get(3).setActive(false);
			gs.get(2).setActive(false);
			gs.get(4).setActive(false);
			
			
			mainFrame.validate();
			processUserInput();
			update();
		}
		
		/**
		 * @return the isInMiniGame boolean to determine whether game is in the mini-game state
		 */
		public boolean isInMiniGame() {
			return isInMiniGame;
		}

		/**
		 * @param isInMiniGame sets the boolean of whether the game is in the mini-game state
		 * or not
		 */
		public void setInMiniGame(boolean isInMiniGame) {
			this.isInMiniGame = isInMiniGame;
			
			if(isInMiniGame == true)
			{
				isPaused = false;
				isInMenu = false;
				isGameOver = false;
				isPlaying = false;
				
				changeState( gs.get(2));
				
				currentState.setActive(true);
				gs.get(0).setActive(false);
				gs.get(1).setActive(false);
				gs.get(3).setActive(false);
				gs.get(4).setActive(false);
			}

			mainFrame.validate();
			processUserInput();
			update();
		}

		/**
		 * @param isGameOver to set the the game to end state
		 */
		public void setGameOver(boolean isGameOver) {
			this.isGameOver = isGameOver;
			
			if(isGameOver == true)
			{
				//Since game is in paused state
				changeState( gs.get(4));
				
				//Making sure only paused is true
				isPlaying = (false);
				isInMenu = (false);
				isPaused = false;
				isInMiniGame = false;
				
				currentState.setActive(true);
				
				gs.get(0).setActive(false);
				gs.get(2).setActive(false);
				gs.get(3).setActive(false);
				gs.get(1).setActive(false);

			}

			processUserInput();
			render();
		}

		/**
		 * @return the isGameOver
		 */
		public boolean isGameOver() {
			return isGameOver;
		}

		/**
		 * @return the isRunning
		 */
		public boolean isRunning() {
			return isRunning;
		}

		/**
		 * @param isRunning the isRunning to set
		 */
		public void setRunning(boolean isRunning) {
			this.isRunning = isRunning;
			changeState( null);
		}
		
		private void changeState(GameState state)
		{
			currentState = state;
		}

		/**
		 * @return the treasureFound
		 */
		public boolean isTreasureFound() {
			return treasureFound;
		}

		/**
		 * @param treasureFound the treasureFound to set
		 */
		public void setTreasureFound(boolean treasureFound) {
			this.treasureFound = treasureFound;
		}

		/**
		 * @return the isGameWon
		 */
		public boolean isGameWon() {
			return isGameWon;
		}

		/**
		 * @param isGameWon the isGameWon to set
		 */
		public void setGameWon(boolean isGameWon) {
			this.isGameWon = isGameWon;
		}

		/**
		 * @return the isMultiplayer
		 */
		public boolean isMultiplayer() {
			return isMultiplayer;
		}

		/**
		 * @param isMultiplayer the isMultiplayer to set
		 */
		public void setMultiplayer(boolean isMultiplayer) {
			this.isMultiplayer = isMultiplayer;
		}

		/**
		 * 
		 * @param isRestarted - boolean referring to state of game
		 */
		public void setRestarted(boolean isRestarted) {
			this.isRestarted = isRestarted;
		}

		/**
		 * @return the isBackdoorFound
		 */
		public boolean isBackdoorFound() {
			return isBackdoorFound;
		}

		/**
		 * @param isBackdoorFound the isBackdoorFound to set
		 */
		public void setBackdoorFound(boolean isBackdoorFound) {
			this.isBackdoorFound = isBackdoorFound;
		}
}

