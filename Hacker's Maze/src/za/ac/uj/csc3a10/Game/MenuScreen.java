/**
 * 
 */
package za.ac.uj.csc3a10.Game;

import za.ac.uj.csc3a10.GUI.ScreenPainter;

/**
 * @author Mazibila A.K.
 *
 */
public class MenuScreen extends Screen 
{
	@Override
	public void accept(ScreenPainter painter)
	{
		painter.paint(this);
	}

	@Override
	public void init()
	{

		
	}

}
