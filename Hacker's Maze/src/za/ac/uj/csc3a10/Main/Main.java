package za.ac.uj.csc3a10.Main;

import za.ac.uj.csc3a10.Game.*;
/**
 * @author Mazibila A.K.
 *
 */
public class Main
{
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		Game game = new Game("Hacker's Maze", 800, 600);
		game.init();
		game.start();	
		game.run();
	}
}
