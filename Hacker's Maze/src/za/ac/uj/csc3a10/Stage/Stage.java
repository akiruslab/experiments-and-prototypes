package za.ac.uj.csc3a10.Stage;

import za.ac.uj.csc3a10.Util.AdjMapGraph;
import za.ac.uj.csc3a10.Util.IPosition;
import za.ac.uj.csc3a10.Util.MapVertex;
import za.ac.uj.csc3a10.Util.Position;



/**
 * @author Mazibila A.K.
 */
public  class Stage 
{
	public static final String SMALL_LEVEL = "small";
	public static final String MEDIUM_LEVEL = "medium";
	public static final String LARGE_LEVEL = "large";
	
	final int lgROWS = 15;//Setting number of rows
	final int lgCOLS = 25;//Setting number of columns
	final int mdROWS = 10;//Setting number of rows
	final int mdCOLS = 20;//Setting number of columns
	final int smROWS = 10;//Setting number of rows
	final int smCOLS = 10;//Setting number of columns

	public static final int NORTH = 1;//Setting index for upward-direction 
	public static final int EAST = 2;//Setting index for right-direction
	public static final int WEST = 3;//Setting index for left-direction
	public static final int SOUTH = 4;//Setting index for downward-direction
	
	public static final int NUM_SPRITES = 5;//Setting number of sprites in the game
	public static final int SPRITE_SIZE = 20;//Setting the size of the sprites
	public static final int PATH = 0;//Setting the index for the maze path
	public static final int WALL = 1;//Setting index for maze walls
	public static final int PLAYER = 2;//Setting player avatar index
	public static final int AI = 3;
	public static final int GOAL = 4;//Setting up goal index
	public static final int BACKDOOR = 42;
	public static final int BACKDOOR_FOUND = 6;
	public static final int GOAL_FOUND = 5;
	
	private String levelName;
	private String size;
	private String difficulty;
	
	private int[][] floor;
	private Position backDoorPos;
	private Position goalPosition;
	private int[] direcArray = null;//Seed array for ramndomizing maze
	private Integer floorWidth;
	private Integer floorLength;
	
	private AdjMapGraph<Position, Integer> mazeMap;
	
	/**
	 * 
	 * @param stageName
	 * @param size
	 * @param difficulty
	 */
	public Stage(String stageName, String size, String difficulty)
	{
		direcArray = new int[4];
		this.levelName = (stageName);
		setSize(size);
		floorWidth = lgCOLS;
		floorLength = lgROWS;
		setDifficulty(difficulty);
		initLevel();
		generateMap();
	}

	public void setSeed(int d1, int d2, int d3, int d4)
	{
		direcArray = new int[4];
		direcArray[0] = d1;
		direcArray[1] = d2;
		direcArray[2] = d3;
		direcArray[3] = d4;
	}
	/**
	 * 
	 */
	public void initLevel()
	{
		mazeMap = new AdjMapGraph<>(true);

		if (size == SMALL_LEVEL)
		{
			floor = new int[smROWS][smCOLS];
			floorWidth = smROWS;
			floorLength = smCOLS;
			createLevel(floorWidth, floorLength);
			floor = genMaze(floor, floorWidth, floorLength);
		}
		else if (size == MEDIUM_LEVEL)
		{
			floor = new int[mdROWS][mdCOLS] ;
			floorWidth = mdROWS;
			floorLength = mdCOLS;
			createLevel(floorWidth, floorLength);
			floor = genMaze(floor, floorWidth, floorLength);
		}
		else if(size == LARGE_LEVEL)
		{
			floor = new int[lgROWS][lgCOLS];
			floorWidth = lgROWS;
			floorLength = lgCOLS;
			createLevel(floorWidth, floorLength);
			floor = genMaze(floor, floorWidth, floorLength);
		}
	}

	private void generateMap() 
	{
		MapVertex<Position,Integer> currentPos = null;
		
		for(int row = 0; row < floorWidth; row++)
		{
			for(int col = 0; col < floorLength; col++)
			{
				if(floor[row][col] == PATH || floor[row][col] == PLAYER || floor[row][col] == GOAL)
				{
					currentPos = new MapVertex<Position,Integer>(new 
							Position(row,col), false);
					currentPos = mazeMap.insertVertex(currentPos.getElement());
					currentPos.setWalkable(true);
					currentPos.setDistanceFromEnd(calcDistanceFromEnd(currentPos));
				}
				else
				{
					currentPos = new MapVertex<Position,Integer>(new 
							Position(row,col), false);
					currentPos = mazeMap.insertVertex(currentPos.getElement());
					currentPos.setWalkable(false);
					currentPos.setDistanceFromEnd(calcDistanceFromEnd(currentPos));
				}
			}
		}
		
		for(IPosition<MapVertex<Position, Integer>> v : mazeMap.vertices())
		{
			setNeighbours(v.getElement());
		}
	}


	private int calcDistanceFromEnd(MapVertex<Position, Integer> currentPos) {
		int xDistance = Math.abs(currentPos.getElement().getxPos() - goalPosition.getxPos());
		int yDistance = Math.abs(currentPos.getElement().getyPos() - goalPosition.getyPos());
		
		if(yDistance > xDistance)
			return 14*xDistance + 10*(yDistance-xDistance);
		return 14*yDistance + 10*(xDistance-yDistance);
	}

	/*
	 *Sets the following grid elements to neighbors
	 * 		adjY
	 * 	adjX x,y adjX
	 * 		adjY
	 */
	private void setNeighbours(MapVertex<Position, Integer> v) {
		MapVertex<Position,Integer> neighbour = null;
		
		for (int r  = -1; r <= 1; r++)
		{
			for(int c = -1; c <=1; c++)
			{
				if(r ==  0 && c == 0)
					continue;
				
				int adjX = v.getElement().getxPos() + r;
				int adjY = v.getElement().getyPos() + c;
				
				if(adjX >= 0 && adjX < floorWidth)
				{
					if(adjY >= 0 && adjY < floorLength)
					{
						neighbour = new MapVertex<Position, Integer>(
								new Position(adjX,adjY), false);
						mazeMap.insertEdge(v, neighbour, new Integer(1));
					}
				}
			}
		}	
		System.out.println(mazeMap.toString());
		
	}

	private void createLevel(int rows, int cols)
	{
		for(int row = 0; row < rows; row++)
		{
			for(int col = 0; col < cols; col++)
			{
				
				floor[row][col] = WALL;
			}
		}
	}
	
	private int[][] genMaze(int[][] maze, int ROWS, int COLS)
	{
		int goalX = ROWS-3;
		int goalY = COLS-3;
		int playerX = 1;
		int playerY = 1;
		
		assert(maze != null);
		int r = 1;
		int c = 1;

		maze[r-1][c-1] = PATH;

		recurAllocation(maze,r, c, ROWS, COLS);

		//Placing goal, back-door and player Avatar in maze
		maze[playerX][playerY] = PLAYER;
		maze[playerX][playerY-1] = BACKDOOR;
		backDoorPos = new Position(playerX, playerY);
		maze[goalX][goalY] = GOAL;
		maze[goalX][goalY-1] = AI;
		goalPosition = new Position(goalX, goalY);
		
		return maze;
	}
	
	private void recurAllocation(int[][] maze,int row,int col, int ROWS, int COLS)
	{
		assert(maze != null);
		
			int[] directionArray = {0,0,0,0};

			double num = 0;
			for (int i = 0; i < 4; i++)
			{
				num = Math.random()*100;
				directionArray[i] = (int)num % 4 + 1;
			}			

		for (int i=0; i < 4; i++)
		{
			switch (directionArray[i])
			{
			case NORTH:
				if (row-2 <= 0)
					continue;
				if (maze[row - 2][col] != PATH)
				{
					maze[row-2][col] = PATH;
					maze[row-1][col] = PATH;
					recurAllocation(maze,(row-2),col, ROWS, COLS);
				}
				break;
			case EAST:
				if (col+2 >= COLS-1)
					continue;
				if (maze[row][col+2] != PATH)
				{
					maze[row][col+2] = PATH;
					maze[row][col+1] = PATH;
					recurAllocation(maze,row,col+2, ROWS, COLS);
				}
				break;
			case WEST:
				if (col-2 <= 0)
					continue;
				if (maze[row][col-2] != PATH)
				{
					maze[row][col-2] = PATH;
					maze[row][col-1] = PATH;
					recurAllocation(maze,row, col-2, ROWS, COLS);
				}
				break;
			case SOUTH:
				if (row+2 >= ROWS-1 )
					continue;
				if (maze[row+2][col] !=PATH)
				{
					maze[row+2][col] = PATH;
					maze[row+1][col] = PATH;
					recurAllocation(maze,row+2,col, ROWS, COLS);
				}
				break;
			}
		}
	}
	
	/**
	 * @return the levelName
	 */
	public String getLevelName() {
		return levelName;
	}

	/**
	 * @return the size
	 */
	public String getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}

	/**
	 * @return the difficulty
	 */
	public String getDifficulty() {
		return difficulty;
	}

	/**
	 * @param difficulty the difficulty to set
	 */
	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	/**
	 * @return the floor
	 */
	public int[][] getFloor() {
		return floor;
	}

	/**
	 * @return the floorWidth
	 */
	public Integer getFloorWidth() {
		return floorWidth;
	}

	/**
	 * @return the floorLength
	 */
	public Integer getFloorLength() {
		return floorLength;
	}

	public AdjMapGraph<Position, Integer> getGraph() {
		return mazeMap;
	}

	/**
	 * 
	 * @return the players starting position
	 */
	public MapVertex<Position,Integer> getGraphStart() {
		MapVertex<Position,Integer> graphStart = null;
		
		for(IPosition<MapVertex<Position,Integer>> v : mazeMap.vertices())
		{
			if(v.getElement().getElement().getxPos() == goalPosition.getxPos()
					&& v.getElement().getElement().getyPos() == goalPosition.getyPos())
				graphStart = v.getElement();
		}
		
		return graphStart;
	}

	/**
	 * 
	 * @return the position of the data
	 */
	public MapVertex<Position,Integer> getEnd() {
		MapVertex<Position,Integer> graphEnd = null;
		
		for(IPosition<MapVertex<Position,Integer>> v : mazeMap.vertices())
		{
			if(v.getElement().getElement().getxPos() == backDoorPos.getxPos()
					&& v.getElement().getElement().getyPos() == backDoorPos.getyPos())
				graphEnd = v.getElement();
		}
		
		return graphEnd;
	}
}
