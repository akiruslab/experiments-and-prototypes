/**
 * 
 */
package za.ac.uj.csc3a10.Util;



/**
 * @author Ken
 *
 */
public interface Queue<E>{
	public int size();
	public boolean isEmpty();
	public void enque(E e);
	public E dequeue ();
	public E front();
}
