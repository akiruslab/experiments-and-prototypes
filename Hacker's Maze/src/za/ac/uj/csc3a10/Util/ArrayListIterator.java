/**
 * 
 */
package za.ac.uj.csc3a10.Util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author AKiRU
 *
 */
public class ArrayListIterator<T> implements Iterator<T> 
{
	IArrayList<T> list;
	int index = 0;
	boolean isValid;
	
	public ArrayListIterator(GamingArrayList<T> list) {
		this.list = list;
		isValid = true;
	}
	@Override
	public boolean hasNext() {
		return (!list.isEmpty() && isValid ==true);
	}

	@Override
	public T next() throws NoSuchElementException
	{
		if(index == list.size())
			throw new NoSuchElementException("No more elements");
		return list.get(index++);
	}

	@Override
	public void remove()
	{
		list.remove(index-1);
		index--;
		isValid = false;
	}
}
