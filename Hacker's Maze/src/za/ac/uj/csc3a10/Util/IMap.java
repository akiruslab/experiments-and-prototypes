/**
 * 
 */
package za.ac.uj.csc3a10.Util;

/**
 * @author AKiRU
 *
 */
public interface IMap<K,V> 
{
	/**
	 * 
	 * @param key
	 * @return
	 */
	public V remove(K key);
	/**
	 * 
	 * @param key
	 * @return
	 */
	public V get(K key);
	/**
	 * 
	 * @param key
	 * @param value
	 */
	public void put(K key, V value);
	/**
	 * 
	 * @return
	 */
	public Iterable<K> keys();
	/**
	 * 
	 * @return
	 */
	public Iterable<IPosition<V>> values();
	/**
	 * 
	 * @return
	 */
	public int size();
	/**
	 * 
	 * @return
	 */
	public boolean isEmpty();
}
