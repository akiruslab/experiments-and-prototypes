/**
 * 
 */
package za.ac.uj.csc3a10.Util;

import java.util.Iterator;

/**
 * @author AKiRU
 *
 */
public class LinkedPositionalListIterator<T> implements  Iterator<IPosition<T>> {
	IPositionalList<T> list;
	IPosition<T> pointer;
	IPosition<T> last;
	boolean isValid;
	
	public LinkedPositionalListIterator(IPositionalList<T> list) {
		this.list = list;
		isValid =  true;
		if(list.isEmpty() != true)
			this.pointer = list.first();
	}
	
	@Override
	public boolean hasNext() {
		return pointer != null;
	}

	@Override
	public IPosition<T> next() {
		if(pointer != null)
		{
			last = pointer;
			pointer = list.getNext(pointer);	
		}
		else
			return null;

		return last;
	}

	@Override
	public void remove() {
		if(last != null)
		{
			list.remove(last);
			last = null;
		}
	}

}
