package za.ac.uj.csc3a10.Util;

public interface AdaptablePriotiyQueue<K, V> 
extends PriorityQueue<K, V>
{
	public Entry<K, V> remove(Entry<K, V> e);
	public void replaceKey(Entry<K, V> e, K key)
	throws IllegalArgumentException;
	public void replaceValue(Entry<K, V> e, V value)
	throws IllegalArgumentException;
}
