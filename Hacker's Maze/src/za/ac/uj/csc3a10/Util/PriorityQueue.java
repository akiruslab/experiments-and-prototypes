/**
 * 
 */
package za.ac.uj.csc3a10.Util;

/**
 * @author AKiRU
 *
 */
public interface PriorityQueue<K,V> {
	public int size();
	public boolean isEmpty();
	public Entry<K, V> min();
	public Entry<K, V> insert(K key, V value);
	public Entry<K, V> removeMin();
}
