/**
 * 
 */
package za.ac.uj.csc3a10.Util;

/**
 * @author AKiRU
 *
 */
public class MapVertex<V,E> extends Vertex<V> implements Comparable<MapVertex<V,E>>
{
	private IPosition<MapVertex<V,E>> position;
	private IMap<MapVertex<V,E>, Edge<E>> outNeighbours, inNeighbours;
	private MapVertex<V,E> parent;
	/**
	 * 
	 */
	public MapVertex(V element, boolean isDirectedGraph) 
	{
		super(element, true);
		outNeighbours = new ProbeHashMap<>();
		if(isDirectedGraph)
			inNeighbours = new ProbeHashMap<>();
		else
			inNeighbours = outNeighbours;
	}

	/**
	 * @return the outNeighbours
	 */
	public IMap<MapVertex<V,E>, Edge<E>> getOutNeighbours() {
		return outNeighbours;
	}

	/**
	 * @return the inNeighbours
	 */
	public IMap<MapVertex<V,E>, Edge<E>> getInNeighbours() {
		return inNeighbours;
	}

	/**
	 * @return the position
	 */
	public IPosition<MapVertex<V,E>> getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(IPosition<MapVertex<V,E>> position) {
		this.position = position;
	}

	public MapVertex<V,E> getParent() {
		return this.getParent();
	}
	
	public void setParent(MapVertex<V, E> parent) {
		this.parent = parent;
	}

	@Override
	public int compareTo(MapVertex<V,E> v) {
		if(v.getElement() != null)
		{
			if(this.getElement() == v.getElement())
				return 0;
			else
				return -1;
		}
		else
			return 1;
	}
}
