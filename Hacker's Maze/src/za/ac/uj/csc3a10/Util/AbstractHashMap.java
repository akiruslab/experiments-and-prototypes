/**
 * 
 */
package za.ac.uj.csc3a10.Util;

import java.util.ArrayList;
import java.util.Random;

/**
 * @author AKiRU
 *
 */
public abstract class AbstractHashMap <K,V>implements IMap<K, V>
{
	protected int numEntries =0;
	protected int tableLength;
	private int primeNum;
	private long scale;
	private long shift;
	
	public AbstractHashMap()
	{
		this(23);
	}
	
	public AbstractHashMap(int size)
	{
		this(size, 179425517);
	}
	
	public AbstractHashMap(int size, int primeNumber)
	{
		this.primeNum = primeNumber;
		tableLength = size;
		Random num = new Random();
		shift = num.nextInt(primeNumber);
		scale = num.nextInt(primeNumber-1)+1;
		createTable(tableLength);
	}
	
	
	/**
	 * Calculate a hash for an Integer
	 * @param item the item to hash
	 * @return a hash code for the item
	 */
	protected int hash(K key) 
	{
		return (int) ((Math.abs(key.hashCode()*scale + shift)%primeNum) % tableLength);
	}
	
	@Override
	public V get(K key) {
		return getBucket(hash(key), key);
	}

	@Override
	public V remove(K key) {
		return getBucket(hash(key), key);
	}


	@Override
	public void put(K key, V value) {
		addBucket(hash(key), key, value);
		if(numEntries > tableLength/2)
			resize();
	}

	private void resize() {
		ArrayList<Entry<K, V>> temp = new 
				ArrayList<>(4*tableLength-1);
		for(Entry<K, V> i : temp)
		{
			temp.add(i);
		}
		
		tableLength = temp.size();
		createTable(tableLength);
		
		numEntries = 0;
		
		for(Entry<K, V> i : temp)
		{
			put(i.getKey(), i.getValue());
		}
	}

	protected abstract void createTable(int size);
	protected abstract V  getBucket(int index, K key);
	protected abstract V  addBucket(int index, K key, V value);
	protected abstract V  removeBucket(int index, K key);
}
