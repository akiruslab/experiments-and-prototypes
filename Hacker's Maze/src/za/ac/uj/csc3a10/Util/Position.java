/**
 * 
 */
package za.ac.uj.csc3a10.Util;

/**
 * @author AKiRU
 *
 */
public class Position  {
	private int xPos;
	private int yPos;
	
	public Position(int xPos, int yPos)
	{
		this.xPos = xPos;
		this.yPos = yPos;
	}

	/**
	 * @return the xPos
	 */
	public int getxPos() {
		return xPos;
	}

	/**
	 * @return the yPos
	 */
	public int getyPos() {
		return yPos;
	}
	
	
}
