/**
 * 
 */
package za.ac.uj.csc3a10.Util;

/**
 * @author AKiRU
 *
 */
public class Edge<E> 
{
	private E element;
	
	public Edge(E element)
	{
		this.element = element;
	}
	
	public E getElement()
	{
		return element;
	};
}
