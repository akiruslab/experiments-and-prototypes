/**
 * 
 */
package za.ac.uj.csc3a10.Util;

import java.util.ArrayList;

/**
 * @author AKiRU
 *
 */
public class ProbeHashMap <K,V> extends AbstractHashMap<K, V> 
{
	private Entry<K, V>[] table;
	private final Entry<K, V> NULL = new Entry<>(null,null);
	
	/**
	 * 
	 */
	 public ProbeHashMap(){super();}
	 
	 /**
	  * 
	  * @param size
	  */
	 public ProbeHashMap(int size){super(size);}
	 
	 /**
	  * 
	  * @param size
	  * @param primeNumber
	  */
	 public ProbeHashMap(int size, int primeNumber){
		 super(size,primeNumber);
	}
	 
	@SuppressWarnings("unchecked")
	protected void createTable(int size) {
		table = (Entry<K,V>[]) new Entry[size];
	 }
	 
	@Override
	public int size() {
		return this.tableLength;
	}

	@Override
	public boolean isEmpty() {
		return tableLength == 0;
	}


	@Override
	protected V getBucket(int index, K key) {
		int i = findSlot(index, key);
		if(i < 0)
			return null;
		return table[i].getValue();
	}


	@Override
	protected V addBucket(int index, K key, V value) {
		int i = findSlot(index, key);
		
		if(i >= 0)
		{
			table[i].setValue(value);
			return value;
		}
	
		table[-(i+1)] = new Entry<>(key,value);
		numEntries++;
		return null;
	}


	@Override
	protected V removeBucket(int index, K key) {
		int i = findSlot(index, key);
		
		if(i < 0)
		{
			return null;
		}
		else
		{
			V removedItem = table[i].getValue();
			table[i] = NULL;
			numEntries--;
			return removedItem;
		}	
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterable<K> keys() {
		IPositionalList<K> keySet = new LinkedPositionalList<K>();
		
		for (int i = 0; i < table.length; i++)
		{
				keySet.addLast(table[i].getKey());
		}
		
		return (Iterable<K>)keySet;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterable<IPosition<V>> values() {
		LinkedPositionalList<V> valueSet = new LinkedPositionalList<V>();
		
		for (int i = 0; i < table.length; i++) {
			if (!isAvailable(i))
				valueSet.addLast(table[i].getValue());
		}
		
		return valueSet.posis();
	}
	
	public Iterable<Entry<K,V>> entrySet()
	{
		ArrayList<Entry<K,V>> list = new ArrayList<>();
		
		for(int i = 0; i < tableLength; i++)
		{
			if (!isAvailable(i))
				list.add(table[i]);
		}
		
		return list;
	}
	
	private boolean isAvailable(int index)
	{
		return (table[index] ==null || table[index] == NULL);
	}
	
	private int findSlot(int index, K key)
	{
		int i = -1;
		int j = index;
		
		do
		{
			if(isAvailable(j))
			{
				if(i == -1)
					i = j;
				if (table[j] == null)
					break;
			}
			else if (table[j].getKey().equals(key))
			{
				return j;
			}
			j = (j+1) % this.tableLength;
		}while (j != index);
		return -(i+1);
	}
}
