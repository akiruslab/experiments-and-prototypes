/**
 * 
 */
package za.ac.uj.csc3a10.Util;

import java.util.Iterator;

/**
 * @author Mazibila A.K.
 *
 */
public class GamingArrayList <T> implements IArrayList<T> 
{
	private int size;
	private T[] array;
	private int arrayLength;
	
	/**
	 * Default constructor that initializes the arrayList components.
	 */
	@SuppressWarnings("unchecked")
	public GamingArrayList()
	{
		size = 0;
		array =  (T[]) new Object[0];
		arrayLength = 10;
	}

	/**
	 * Constructor that accepts one parameter for setting the initial
	 * length of the arrayList.
	 * 
	 * @param arrayLength - length of the array of elements.
	 */
	@SuppressWarnings("unchecked")
	public GamingArrayList(int arrayLength)
	{
		this.size = 0;
		this.arrayLength = arrayLength;
		this.array =  (T[]) new Object[arrayLength];
	}
	
	/* (non-Javadoc)
	 * @see za.ac.uj.csc2a10.Util.List#get(int)
	 */
	@Override
	public T get(int index) {
		if(index < size || index>size)
		{
			return array[index];
		}
		else
		{
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see za.ac.uj.csc2a10.Util.List#set(int, java.lang.Object)
	 */
	@Override
	public void set(int index, T element) 
	{
		if(index < size || index>size)
		{
			array[index] = element;
		}
	}

	/* (non-Javadoc)
	 * @see za.ac.uj.csc2a10.Util.List#add(int, java.lang.Object)
	 */
	@Override
	public void add(int index, T element) 
	{
		T[] tempArray = null;
		if(size >= arrayLength && index < arrayLength-1 && index >= 0)
		{
			//Resize array
			arrayLength = arrayLength+5;
			tempArray = cloneArray(array, arrayLength);
			  
			addElementAndShift(index, element, tempArray);
		}
		else
		{
			tempArray =cloneArray(array, array.length);
			addElementAndShift(index, element, tempArray);
		}
	}
	@SuppressWarnings("unchecked")
	private T[] cloneArray(T[] array, int size)
	{
		T[] tempArray = (T[]) new Object[size];
		
		for (int i = 0 ; i < array.length ; i++)
		{
			tempArray[i] = array[i];
		}
		
		return tempArray;
	}
	private void addElementAndShift(int index, T element, T[] tempArray)
	{
		//Add element and shift elements up
		for (int i = index ; i < array.length-1 ; i++)
		{
			tempArray[i+1] = array[i];
		}
		tempArray[index] = element;
		
		array = tempArray;
		
		size++;
	}

	/* (non-Javadoc)
	 * @see za.ac.uj.csc2a10.Util.List#remove(int)
	 */
	@Override
	public T remove(int index) {
		T element  = null;
		if(index < size || index>size)
		{
			//Removing element and restructuring array of elements
			element =array[index];
			
			for (int i = index ; i < array.length-1 ; i++)
			{
				array[i] = array[i+1];
			}

			size--;
			return element;
		}
		else
		{
			return element;
		}
	}

	/* (non-Javadoc)
	 * @see za.ac.uj.csc2a10.Util.List#size()
	 */
	@Override
	public int size() {
		return size;
	}

	/* (non-Javadoc)
	 * @see za.ac.uj.csc2a10.Util.List#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		if(size == 0)
			return true;
		else
			return false;
	}

	@Override
	public Iterator<T> iterator() {
		return new ArrayListIterator<T>(this);
	}
}
