/**
 * 
 */
package za.ac.uj.csc3a10.Util;

import java.util.Comparator;

/**
 * @author AKiRU
 *
 */
public class DefaultComparator<T> implements Comparator<T>
{
	@SuppressWarnings("unchecked")
	@Override
	public int compare(T key1, T key2) {
		return ((Comparable<T>)key1).compareTo(key2);
	}
	
}