/**
 * 
 */
package za.ac.uj.csc3a10.Util;

import java.util.Comparator;

/**
 * @author AKiRU
 *
 */
public abstract class AbstractPriorityQueue<K,V> implements PriorityQueue<K, V> {
	private Comparator<K> keyComparator;
	
	/**
	 * 
	 * @param comp
	 */
	protected AbstractPriorityQueue(Comparator<K> comp) {
		keyComparator = comp;
	}
	
	protected int compare(Entry<K,V> e1, Entry<K,V> e2)
	{
		return keyComparator.compare(e1.getKey(), e2.getKey());
	}
	
	protected boolean validate(K key)throws IllegalArgumentException
	{
		try
		{
			return (keyComparator.compare(key, key)==0);
		}
		catch (ClassCastException e)
		{
			throw new IllegalArgumentException("Wrong key");
		}
	}
}
