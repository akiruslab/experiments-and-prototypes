/**
 * 
 */
package za.ac.uj.csc3a10.Util;


/**
 * @author AKiRU
 *
 */
public interface IPositionalList <T>
{
	/**
	 * 
	 * @return
	 */
	public IPosition<T> first();
	/**
	 * 
	 * @return
	 */
	public IPosition<T> last();
	
	/**
	 * 
	 * @param pos
	 * @return
	 */
	public IPosition<T> getNext(IPosition<T> pos);
	
	/**
	 * 
	 * @param pos
	 * @return
	 */
	public IPosition<T> getPrev(IPosition<T> pos);
	
	/**
	 * 
	 * @param pos
	 * @param element
	 * @return
	 */
	public IPosition<T> addAfter(IPosition<T> pos, T element);
	
	/**
	 * 
	 * @param pos
	 * @param element
	 * @return
	 */
	public IPosition<T> addBefore(IPosition<T> pos, T element);
	
	/**
	 * 
	 * @param element
	 * @return
	 */
	public IPosition<T> addFirst(T element);
	
	/**
	 * 
	 * @param element
	 * @return
	 */
	public IPosition<T> addLast(T element);
	
	/**
	 * 
	 * @param pos
	 * @return
	 */
	public T remove(IPosition<T> pos);
	
	/**
	 * 
	 * @return
	 */
	public boolean isEmpty();
	Integer size();

}
