/**
 * 
 */
package za.ac.uj.csc3a10.Util;


/**
 * @author AKiRU
 *
 */
public  interface  Graph <V,E>{
	
	/**
	 * 
	 * @return
	 */
	public int numVertices();
	/**
	 * 
	 * @return
	 */
	public Iterable<IPosition<MapVertex<V,E>>>vertices();
	/**
	 * 
	 * @param vertex
	 * @return
	 */
	public MapVertex<V,E> removeVertex(MapVertex<V,E> vertex);
	/**
	 * 
	 * @param edge
	 * @return
	 */
	public MapVertex<V,E>[] getEndVertices(Edge<E> edge);
	/**
	 * 
	 * @param element
	 * @return
	 */
	public MapVertex<V,E> insertVertex(V element);
	/**
	 * 
	 * @param vertexV
	 * @param edge
	 * @return
	 */
	public MapVertex<V,E> getOpposite(MapVertex<V,E> vertexV, Edge<E> edge);
	
	/**
	 * 
	 * @param vertexV
	 * @return
	 */
	public int outDegree(MapVertex<V,E> vertexV);
	/**
	 * 
	 * @param vertexV
	 * @return
	 */
	public int inDegree(MapVertex<V,E> vertexV);
	
	/**
	 * 
	 * @return
	 */
	public int numEdges();
	/**
	 * 
	 * @return
	 */
	public Iterable<IPosition<Edge<E>>> edges();
	/**
	 * 
	 * @param edge
	 * @return
	 */
	public Edge<E> removeEdge(Edge<E> edge);
	/**
	 * 
	 * @param vertexU
	 * @param vertexV
	 * @return
	 */
	public Edge<E> getEdge(MapVertex<V,E> vertexU, MapVertex<V,E> vertexV);
	/**
	 * 
	 * @param vertexU
	 * @param vertexV
	 * @param element
	 * @return
	 */
	public Edge<E> insertEdge(MapVertex<V,E> vertexU, MapVertex<V,E> vertexV, E element);
	
	/**
	 * 
	 * @param vertex
	 * @return
	 */
	public Iterable<IPosition<Edge<E>>> outgoingEdges(MapVertex<V,E> vertexV);
	/**
	 * 
	 * @param vertexV
	 * @return
	 */
	public Iterable<IPosition<Edge<E>>> incomingEdges(MapVertex<V,E> vertexV);

}
