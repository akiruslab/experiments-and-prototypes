/**
 * 
 */
package za.ac.uj.csc3a10.Util;

/**
 * @author AKiRU
 *
 */
@SuppressWarnings({ "unchecked" })
public class AdjMapGraph<V,E> implements Graph<V, E>
{
	private boolean isUnDirected;
	private LinkedPositionalList<MapVertex<V,E>>listOfVertices = new LinkedPositionalList<>();
	private LinkedPositionalList<Edge<E>>listOfEdges = new LinkedPositionalList<>();
	/**
	 * 
	 */
	public AdjMapGraph(boolean unDirected) {
		isUnDirected = unDirected;
	}

	@Override
	public int numVertices() {
		return listOfVertices.size();
	}

	@Override
	public MapVertex<V,E> removeVertex(MapVertex<V,E> vertex) {
		MapVertex<V,E> mV = vertex;
		//Removing all edges from graph related to vertex
		for(IPosition<Edge<E>> edge : mV.getOutNeighbours().values())
		{
			removeEdge(edge.getElement());
		}
		for(IPosition<Edge<E>> edge : mV.getInNeighbours().values())
		{
			removeEdge(edge.getElement());
		}
		
		return listOfVertices.remove(mV.getPosition());
	}

	@Override
	public MapVertex<V,E>[] getEndVertices(Edge<E> edge) {
		MapEdge<E> e = convert(edge);
		return e.getArrayOfEndVertices();
	}

	@Override
	public MapVertex<V,E> insertVertex(V element) {
		MapVertex<V,E> mVert = new MapVertex<V,E>(element, isUnDirected);
		mVert.setPosition(listOfVertices.addLast(mVert));
		return mVert;
	}

	@Override
	public MapVertex<V,E> getOpposite(MapVertex<V,E> current, Edge<E> edge) throws IllegalArgumentException
	{
		MapEdge <E> e = convert(edge);
		MapVertex<V,E>[] endVertices = e.getArrayOfEndVertices();
		if(endVertices[1] == current)
		{
			return endVertices[0];
		}
		else if(endVertices[0] == current)
		{
			return endVertices[1];
		}
		else throw new IllegalArgumentException("Egde doesn't match vertex");
	}

	@Override
	public int outDegree(MapVertex<V,E> vertexV) {
		MapVertex<V,E> v = (vertexV);
		
		return v.getOutNeighbours().size();
	}

	@Override
	public int inDegree(MapVertex<V,E> vertexV) {
		MapVertex<V,E> v = (vertexV);
		
		return v.getInNeighbours().size();
	}

	@Override
	public int numEdges() {
		return listOfEdges.size();
	}
	
	@Override
	public Edge<E> getEdge(MapVertex<V,E> vertexU, MapVertex<V,E> vertexV) {
		MapVertex<V,E>startPoint = (vertexU);
		
		return startPoint.getInNeighbours().get(vertexV);
	}

	@Override
	public Edge<E> insertEdge(MapVertex<V,E> vertexU, MapVertex<V,E> vertexV, E element)
		throws IllegalArgumentException
	{
		if(getEdge(vertexU, vertexV) != null)
		{
			throw new IllegalArgumentException("Edge already exists");
		}
		else //Insert
		{
			MapEdge<E> e = new MapEdge<E>(vertexU, vertexV, element);
			e.setPosition(listOfEdges.addLast(e));
			//Connect edge to vertices
			MapVertex<V,E> start = (vertexU);
			MapVertex<V,E> end = (vertexV);
			start.getOutNeighbours().put(vertexV, e);
			end.getInNeighbours().put(vertexU, e);
			return e;
		}
	}
	
	@Override
	public Edge<E> removeEdge(Edge<E> edge) {
		MapEdge<E> mV = convert(edge);
		
		return listOfEdges.remove(mV.position);
	}
	
	@Override
	public Iterable<IPosition<Edge<E>>> outgoingEdges(MapVertex<V,E> current) {
		MapVertex<V,E> v = (current);
		
		return v.getOutNeighbours().values();
	}

	@Override
	public Iterable<IPosition<Edge<E>>> incomingEdges(MapVertex<V,E> vertexV) {
		MapVertex<V,E> v = (vertexV);
		
		return v.getInNeighbours().values();
	}

	@Override
	public Iterable<IPosition<MapVertex<V, E>>> vertices() {
		return  listOfVertices.posis();
	}

	@Override
	public Iterable<IPosition<Edge<E>>> edges() {
		return  listOfEdges.posis();
	}

	public String toString()
	{
		String output = "Printing out graph representation:\n";
		int currentRow = 0;
		Position pos = (Position) listOfVertices.first().getElement().getElement();
		
		for(IPosition<MapVertex<V,E>> vert : vertices())
		{
			pos =  (Position) vert.getElement().getElement();
			if(currentRow < pos.getxPos())
			{
				output+= "\n";
				currentRow++;
			}
			
			if(vert.getElement().isWalkable())
				output+=  "0";
			else
				output+= "#";
		}
		
		return output;
	}
	
	private MapEdge<E> convert(Edge<E> edge)throws IllegalArgumentException
	{
		if(edge != null && edge instanceof Edge)
			return (MapEdge<E>) edge;
		else
			throw new IllegalArgumentException("");
	}

	private class MapEdge<E>extends Edge<E>
	{
		private IPosition<Edge<E>> position;
		private MapVertex<V,E>[] arrayOfEndVertices;
		
		/**
		 * 
		 */
		public MapEdge(MapVertex<V,E> vertex1, MapVertex<V,E> vertex2, E element) {
			super(element);
			arrayOfEndVertices = (MapVertex<V,E>[]) new MapVertex[]{vertex1,vertex2};
		}

		/**
		 * @return the position
		 */
		public IPosition<Edge<E>> getPosition() {
			return position;
		}

		/**
		 * @param position the position to set
		 */
		public void setPosition(IPosition<Edge<E>> position) {
			this.position = position;
		}

		/**
		 * @return the arrayOfEndVertices
		 */
		public MapVertex<V,E>[] getArrayOfEndVertices() {
			return arrayOfEndVertices;
		}

		/**
		 * @param arrayOfEndVertices the arrayOfEndVertices to set
		 */
		public void setArrayOfEndVertices(MapVertex<V,E>[] arrayOfEndVertices) {
			this.arrayOfEndVertices = arrayOfEndVertices;
		}

	}
}
