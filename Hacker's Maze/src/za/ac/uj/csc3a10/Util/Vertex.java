/**
 * 
 */
package za.ac.uj.csc3a10.Util;

/**
 * @author AKiRU
 *
 */
public abstract  class Vertex<V>{
	private boolean isWalkable;
	private V element; 
	private int distanceFromEnd;
	private int gCost;
	private int hCost;
	
	/**
	 * @return the isWalkable
	 */
	public boolean isWalkable() {
		return isWalkable;
	}
	/**
	 * @param isWalkable the isWalkable to set
	 */
	public void setWalkable(boolean isWalkable) {
		this.isWalkable = isWalkable;
	}
	
	public Vertex(V position, boolean isWalkable)
	{
		element = position;
		this.isWalkable = isWalkable;
	}
	/**
	 * @return the gCost
	 */
	public int get_gCost() {
		return gCost;
	}
	/**
	 * @param gCost the gCost to set
	 */
	public void set_gCost(int gCost) {
		this.gCost = gCost;
	}
	/**
	 * @return the fCost
	 */
	public Integer get_hCost() {
		return hCost;
	}
	/**
	 * @param fCost the fCost to set
	 */
	public void set_hCost(int hCost) {
		this.hCost = hCost;
	}
	
	public Integer get_fCost()
	{
		return gCost+hCost;
	}
	
	public  V getElement()
	{
		return element;
	}
	/**
	 * @return the distanceFromEnd
	 */
	public int getDistanceFromEnd() {
		return distanceFromEnd;
	}
	/**
	 * @param distanceFromEnd the distanceFromEnd to set
	 */
	public void setDistanceFromEnd(int distanceFromEnd) {
		this.distanceFromEnd = distanceFromEnd;
	}

	public int compareTo(Vertex<V> vertex) {
		int result = vertex.get_fCost().compareTo(this.get_fCost());
		if (result == 0)
		{
			result = vertex.get_hCost().compareTo(this.get_hCost());
		}
		return result;
	}
}
