/**
 * 
 */
package za.ac.uj.csc3a10.Util;

/**
 * @author AKiRU
 *
 */
public interface IList<T>
{
	/**
	 * 
	 * @param p
	 * @param item
	 * @return
	 */
	public IPosition<T> addAfter(IPosition<T> p, T item);
	/**
	 * 
	 * @param p
	 * @param item
	 * @return
	 */
	public IPosition<T> addBefore(IPosition<T> p, T item);
	/**
	 * 
	 * @param item
	 * @return
	 */
	public IPosition<T> addFirst(T item);
	/**
	 * 
	 * @param item
	 * @return
	 */
	public IPosition<T> addLast(T item);
	/**
	 * 
	 * @param p
	 * @return
	 */
	public T remove(IPosition<T> p);
	
	/**
	 * 
	 * @param p
	 * @return
	 */
	public IPosition<T> getNext(IPosition<T> p);
	/**
	 * 
	 * @param p
	 * @return
	 */
	public IPosition<T> getPrev(IPosition<T> p);
	/**
	 * 
	 * @return
	 */
	public IPosition<T> first();
	/**
	 * 
	 * @return
	 */
	public IPosition<T> last();
	
	/**
	 * 
	 * @return
	 */
	public boolean isEmpty();
	/**
	 * 
	 * @return
	 */
	public Integer size();
}
