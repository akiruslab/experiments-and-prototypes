/**
 * 
 */
package za.ac.uj.csc3a10.Util;

/**
 * @author Mazibila A.K.
 *
 */
public interface IArrayList <T>extends Iterable<T>
{
	/**
	 * Accessor method that returns the element at the index.
	 * Accepts a integer parameter.
	 * 
	 * @param index - integer value representing index to retrieve
	 * @return T - element at current index
	 */
	public T get(int index);
	
	/**
	 * Mutator method that sets the value of an element  at a specific
	 * index.
	 * Accepts 2 parameters.
	 * 
	 * @param index - integer representing index at which element will
	 * be changed.
	 * @param element - variable to set as current element.
	 */
	public void set(int index, T element);
	
	/**
	 *	Mutator method that adds an element  at a specific
	 * index.
	 * Accepts 2 parameters.
	 * 
	 * @param index - integer representing index at which element will
	 * be added.
	 * @param variable to add at given index.
	 */
	public void add(int index, T element);
	
	/**
	 * Function to remove an element at a given index.
	 * Accepts one integer parameter.
	 * 
	 * @param index - position of element to remove.
	 * @return T - element at the given index.
	 */
	public T remove(int index);
	
	/**
	 * Function that returns the size of the list.
	 * 
	 * @return size of the list.
	 */
	public int size();
	
	/**
	 * Function that returns whether or not the list is empty.
	 * 
	 * @return boolean that states whether the list is empty 
	 * or not.
	 */
	public boolean isEmpty();
}
