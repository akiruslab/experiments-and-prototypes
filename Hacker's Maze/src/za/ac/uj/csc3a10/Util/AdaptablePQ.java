/**
 * 
 */
package za.ac.uj.csc3a10.Util;

import java.util.Comparator;

/**
 * @author AKiRU
 *
 */
public class AdaptablePQ<K,V> extends HeapPriorityQueue<K, V> 
	implements AdaptablePriotiyQueue<K,V>
{
	public AdaptablePQ() {
		super();
	}
	
	public AdaptablePQ(Comparator<K> comparator) {
		super(comparator);
	}
	
	protected AdapatablePQEntry<K, V> convert(Entry<K, V> entry)
	throws IllegalArgumentException
	{
		if((entry instanceof AdapatablePQEntry) == false)
			throw new IllegalArgumentException("Not right type of entry");
		
		AdapatablePQEntry<K, V> e = (AdapatablePQEntry<K,V>) entry;
		
		int i = e.getIndex();
		if(i >= heap.size() || heap.get(i) != e)
			throw new IllegalArgumentException("Entry invalid");
		
		
		return e;
	}
	
	protected void swapElements(int e1, int e2)
	{
		super.swapElements(e1, e2);
		
		((AdapatablePQEntry<K, V>)heap.get(e1)).setIndex(e2);
		
		((AdapatablePQEntry<K, V>)heap.get(e2)).setIndex(e1);
	}
	
	protected void bubbleSort(int element)
	{
		if(element > 0 && compare(heap.get(element),
			heap.get(getParent(element))) <0)
		{
			upheap(element);
		}
		else
			downheap(element);
	}
	
	public Entry<K, V> insert(K key, V value)
	throws IllegalArgumentException
	{
		validate(key);
		
		Entry<K, V> newEntry = new AdapatablePQEntry<>(key,
				value, heap.size());
		heap.add(newEntry);
		upheap(heap.size()-1);
		
		return newEntry;
	}
	
	@Override
	public Entry<K, V> remove(Entry<K, V> e) {
		AdapatablePQEntry<K, V> entry = convert(e);

		int index = entry.getIndex();
		int lastIndex = heap.size()-1;
		
		if(index == lastIndex)
		{
			heap.remove(lastIndex);
		}
		else
		{
			swapElements(index, lastIndex);
			heap.remove(lastIndex);
			bubbleSort(index);
		}
		
		return null;
	}

	@Override
	public void replaceKey(Entry<K, V> e, K key) 
	{
		AdapatablePQEntry<K, V> entry = convert(e);
		validate(key);
		
		entry.setKey(key);
		
		bubbleSort(entry.getIndex());
	}

	@Override
	public void replaceValue(Entry<K, V> e, V value) 
	{
		AdapatablePQEntry<K, V> entry = convert(e);
		entry.setValue(value);
	}

	protected static class AdapatablePQEntry<K,V> extends Entry<K, V>
	{
		private int index;
		
		public AdapatablePQEntry(K key, V value, int index) {
			super(key, value);
			this.index =index;
		}

		public int getIndex() {
			return index;
		}

		public void setIndex(int index) {
			this.index = index;
		}
		
	}

	public boolean contains(MapVertex<Position, Integer> neighbour) {
		return heap.contains(neighbour);
	}
}
