/**
 * 
 */
package za.ac.uj.csc3a10.Util;

import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;

/**
 * @author AKiRU
 *
 */
public class PathFinder
{
	private AdjMapGraph<Position,Integer> grid;
	private MapVertex<Position,Integer> startPoint;
	private MapVertex<Position,Integer> goal;
	private List<MapVertex<Position,Integer>> path = null;
	
	public PathFinder(AdjMapGraph<Position,Integer> world, MapVertex<Position,Integer> start, 
			MapVertex<Position,Integer> end)
	{
		grid = world;
		startPoint = start;
		goal = end;
	}
	
	public List<MapVertex<Position,Integer>> findPath()
	{
		AdaptablePQ <Integer,MapVertex<Position,Integer>> open = new AdaptablePQ();
		HashSet<MapVertex<Position,Integer>> closed = new HashSet<>();
		
		open.insert(startPoint.getDistanceFromEnd(), startPoint);
		
		MapVertex<Position,Integer> current = null;
		while(open. size()> 0)
		{
			System.out.println("Open not empty yet...");
			current = open.removeMin().getValue();
			closed.add(current);
			
			if(current == goal)
			{
				retracePath();
				return path;
			}
			
			for(IPosition<Edge<Integer>> edge : grid.outgoingEdges(current))
			{
				System.out.println("Checking neighbours of " + "Vertex with distanceFromGoal = "+ current.getDistanceFromEnd());
				MapVertex<Position,Integer> neighbour = grid.getOpposite(current, edge.getElement());
				
				if(!neighbour.isWalkable() || closed.contains(neighbour))
					continue;
				
				int moveCost = current.get_gCost() + edge.getElement().getElement();
				
				if(moveCost < neighbour.get_gCost() || !open.contains(neighbour))
				{
					neighbour.set_gCost(moveCost);
					neighbour.set_hCost(neighbour.getDistanceFromEnd());
					neighbour.setParent(current);   
					
					if(!open.contains(neighbour) )
					{
						System.out.println("adding to openset");
						open.insert(neighbour.getDistanceFromEnd(),neighbour); 
					}
					else
					{
						System.out.println("Updating openset");
						open.replaceValue(new Entry<Integer, MapVertex<Position,Integer>>(
								neighbour.getDistanceFromEnd(),neighbour), neighbour);
					}
				}
			}
		}
		return null;
	}

	private void retracePath() 
	{
		MapVertex<Position,Integer> current = goal;
		LinkedQueue<MapVertex<Position,Integer>> reversePath = new LinkedQueue<>();
		while(current != startPoint)
		{
			reversePath.enque(current);
			current = current.getParent();
		}
		
		for(int i = 0; i < reversePath.size(); i++)
		{
			path.add(reversePath.dequeue());
		}
		
	}
}
