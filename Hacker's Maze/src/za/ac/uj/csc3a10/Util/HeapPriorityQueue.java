/**
 * 
 */
package za.ac.uj.csc3a10.Util;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * @author AKiRU
 *
 */
public class HeapPriorityQueue<K,V> extends AbstractPriorityQueue<K, V> {

	protected ArrayList<Entry<K,V>> heap = new ArrayList<>();
	
	public HeapPriorityQueue(Comparator<K> comp) {
		super(comp);
	}
	
	public HeapPriorityQueue() {
		super(new DefaultComparator<>());
	}
	
	protected int getParent(int element)
	{
		return (element-1)/2;
	}
	
	protected int getLeft(int element)
	{
		return (2*element + 1);
	}
	
	protected int getRight(int element)
	{
		return (2*element + 2);
	}
	
	protected boolean hasLeft(int element)
	{
		return getLeft(element) < heap.size();
	}
	
	protected boolean hasRight(int element)
	{
		return getRight(element) < heap.size();
	}
	
	protected void swapElements (int element1, int element2)
	{
		Entry<K, V> e = heap.get(element1);
		heap.set(element1, heap.get(element2));
		heap.set(element2, e);
	}
	
	protected void upheap(int element)
	{
		int parent;
		while (element > 0)
		{
			parent = getParent(element);
			if(compare(heap.get(element), 
					heap.get(parent))>=0)
				break;
			swapElements(element, parent);
			element = parent;
		}
	}
	
	protected void downheap(int element)
	{
		int left;
		int smallestChild;
		int right;
		while(hasLeft(element) == true)
		{
			smallestChild =left = getLeft(element);
			
			if(hasRight(element))
			{
				right = getRight(element);
				if(compare(heap.get(left),
						heap.get(right))>0)
					smallestChild = right;
			}
			
			if(compare(heap.get(smallestChild),
						heap.get(element))>=0)
				break;
			
			swapElements(element, smallestChild);
			element = smallestChild;
		}
	}

	@Override
	public int size() {
		return heap.size();
	}

	@Override
	public boolean isEmpty() {
		return heap.isEmpty();
	}

	@Override
	public Entry<K, V> min() {
		if(heap.isEmpty() == false)
			return null;
		return heap.get(0);
	}

	@Override
	public Entry<K, V> insert(K key, V value)
	throws IllegalArgumentException
	{
		validate(key);
		
		Entry<K, V> newEntry = new Entry<>(key,value);
		heap.add(newEntry);
		upheap(heap.size() -1);
		
		return newEntry;
	}

	@Override
	public Entry<K, V> removeMin() {
		if(heap.isEmpty())
		return null;
		
		Entry<K, V> entryToRemove = heap.get(0);
		swapElements(0, heap.size()-1);
		downheap(0);
		
		return entryToRemove;
	}

}
