/**
 * 
 */
package za.ac.uj.csc3a10.Util;

/**
 * @author AKiRU
 *
 */
public interface IPosition<T> {
	/**
	 * 
	 * @return
	 */
	public T getElement();
}
