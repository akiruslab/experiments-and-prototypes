/**
 * 
 */
package za.ac.uj.csc3a10.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ken
 *
 */
public class LinkedQueue<E> implements Queue<E> {
	private List<E> list = new ArrayList<>();
	@Override
	public int size() {
		return list.size();
	}

	@Override
	public boolean isEmpty() {
		return list.isEmpty();
	}

	@Override
	public void enque(E e) {
		list.add(e);
	}

	@Override
	public E dequeue() {
		return list.remove(0);
	}

	@Override
	public E front() {
		return list.get(0);
	}

}
