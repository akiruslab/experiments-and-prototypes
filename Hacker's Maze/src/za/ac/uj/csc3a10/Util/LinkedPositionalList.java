/**
 * 
 */
package za.ac.uj.csc3a10.Util;

import java.util.Iterator;

/**
 * @author AKiRU
 *
 */
public class LinkedPositionalList<T> implements IPositionalList<T>
{
	int size = 0;
	Node<T> head;
	Node<T> tail;
	
	public LinkedPositionalList() {
		head = new Node<>(null,null,null);
		tail = new Node<>(null, head, null);
		head.setNext(tail);
	}
	
	@Override
	public IPosition<T> first() {
		return search(head.getNext());
	}

	@Override
	public IPosition<T> last() {
		return search(tail.getNext());
	}

	@Override
	public IPosition<T> getNext(IPosition<T> pos) {
		Node<T> node = convert(pos);
		return search(node.getNext());
	}

	@Override
	public IPosition<T> getPrev(IPosition<T> pos) {
		Node<T> node = convert(pos);
		return search(node.getPrev());
	}

	@Override
	public IPosition<T> addAfter(IPosition<T> pos, T element) {
		Node<T> node = convert(pos);
		return addBetween(element, node, node.getNext());
	}

	@Override
	public IPosition<T> addBefore(IPosition<T> pos, T element) {
		Node<T> node = convert(pos);
		return addBetween(element, node.getPrev(), node);
	}

	@Override
	public IPosition<T> addFirst(T element) {
		return addBetween(element, head, head.getNext());
	}

	@Override
	public IPosition<T> addLast(T element) {
		return addBetween(element, tail.getPrev(), tail);
	}

	@Override
	public T remove(IPosition<T> pos) {
		Node<T> node = convert(pos);
		Node<T> next = node.getNext();
		Node<T> prev = node.getPrev();
		
		prev.setNext(next);
		next.setPrev(prev);
		
		T removedElement = node.getElement();
		node.setElement(null);
		node.setNext(null);
		node.setPrev(null);
		size--;
		return removedElement;
	}

	public IPosition<T> search(Node<T> node) {
		if(node == head || node == tail)
			return null;
		else
			return node;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	private class IterablePostion implements Iterable<IPosition<T>>
	{
		@Override
		public Iterator<IPosition<T>> iterator() {
			return new LinkedPositionalListIterator<T>(LinkedPositionalList.this);
		}

	}
	
	public Iterable<IPosition<T>> posis()
	{
		return new IterablePostion();
	}

	@Override
	public Integer size() {
		return size;
	}
	
	private IPosition<T> addBetween(T element, Node<T> prev, Node<T> next)
	{
		Node<T> newNode = new Node<>(element,prev,next);
		
		prev.setNext(newNode);
		next.setPrev(newNode);
		
		size++;
		return newNode;
	}
	private Node<T> convert(IPosition<T> pos)throws IllegalArgumentException
	{
		Node<T> node = null;
		if((pos instanceof Node) == true)
			node = (Node<T>) pos;
		else
			throw new IllegalArgumentException();
		
		 if(node.getNext() == null)
			 throw new IllegalArgumentException();
	
		 return node;
	}
	
	private static class Node<E> implements IPosition<E>
	{
		private E element;
		private Node<E> prev;
		private Node<E> next;
		
		public Node(E element, Node<E> prev, Node<E> next)
		{
			this.element = element;
			this.prev = prev;
			this.next = next;
		}

		@Override
		public E getElement()  throws IllegalStateException
		{
			if(next == null)
				throw new IllegalStateException("Element non-existent");
			return element;
		}

		/**
		 * @return the prev
		 */
		public Node<E> getPrev() {
			return prev;
		}

		/**
		 * @param prev the prev to set
		 */
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

		/**
		 * @return the next
		 */
		public Node<E> getNext() {
			return next;
		}

		/**
		 * @param next the next to set
		 */
		public void setNext(Node<E> next) {
			this.next = next;
		}

		/**
		 * @param element the element to set
		 */
		public void setElement(E element) {
			this.element = element;
		}
	}

}
