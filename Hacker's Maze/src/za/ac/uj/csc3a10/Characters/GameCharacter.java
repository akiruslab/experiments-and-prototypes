package za.ac.uj.csc3a10.Characters;

import za.ac.uj.csc3a10.GUI.Animation;
import za.ac.uj.csc3a10.Stage.Stage;

/**
 * @author Mazibila A.K.
 *
 */
public abstract class GameCharacter
{
	private String name;
	private float speed;
	private int xPos;
	private int yPos;
	private int direction;
	Animation a;
	
	/**
	 * 
	 */
	public GameCharacter(){}
	
	/**
	 * 
	 * @param name
	 * @param speed
	 * @param xPos
	 * @param yPos
	 */
	public GameCharacter(String name, float speed, int xPos, int yPos)
	{
		this.name = name;
		this.speed = 0;
		this.xPos = xPos;
		this.yPos =yPos;
		direction = 0;
		a = new Animation();
	}
	
	/**
	 * 
	 */
	public void moveRight()
	{
		xPos += 1;
		direction = Stage.EAST;
	}
	
	/**
	 * 
	 */
	public void moveUP() 
	{
		yPos -= 1;
		direction = Stage.NORTH;
	}

	/**
	 * 
	 */
	public void moveLeft()
	{
		xPos -=1;
		direction = Stage.WEST;
	}

	/**
	 * 
	 */
	public void moveDown() 
	{
		yPos += 1;
		direction = Stage.SOUTH;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the speed
	 */
	public float getSpeed() {
		return speed;
	}
	/**
	 * @param speed the speed to set
	 */
	public void setSpeed(float speed) {
		this.speed = speed;
	}
	/**
	 * @return the xPos
	 */
	public int getxPos() {
		return xPos;
	}
	/**
	 * @param xPos the xPos to set
	 */
	public void setxPos(int xPos) {
		this.xPos = xPos;
	}
	/**
	 * @return the yPos
	 */
	public int getyPos() {
		return yPos;
	}
	/**
	 * @param yPos the yPos to set
	 */
	public void setyPos(int yPos) {
		this.yPos = yPos;
	}

	/**
	 * @return the direction
	 */
	public int getDirection() {
		return direction;
	}

	/**
	 * @param direction the direction to set
	 */
	public void setDirection(int direction) {
		this.direction = direction;
	}
}
