package za.ac.uj.csc3a10.Characters;

import java.io.Serializable;

/**
 * Class representing single player;
 * 
 * @author Mazibila A.K.
 *
 */
public class Player extends GameCharacter implements Serializable
{
	private static final long serialVersionUID = -5256824268048556002L;
	private int age;
	private String favouriteColour;
	
	private String bestTime;
	
	public Player()
	{
	
	}
	/**
	 * 
	 * @param name
	 * @param age
	 * @param favoriteColour
	 * @param speed
	 * @param xPos
	 * @param yPos
	 */
	public Player(String name,int age, String favoriteColour, float speed, int xPos, int yPos)
	{
		super(name, speed, xPos, yPos);
		this.age = age;
		this.favouriteColour = favoriteColour;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @return the favouriteColour
	 */
	public String getFavouriteColour() {
		return favouriteColour;
	}
	/**
	 * @param favouriteColour the favouriteColour to set
	 */
	public void setFavouriteColour(String favouriteColour) {
		this.favouriteColour = favouriteColour;
	}
	/**
	 * @return the bestTime
	 */
	public String getBestTime() {
		return bestTime;
	}
	/**
	 * @param bestTime the bestTime to set
	 */
	public void setBestTime(String bestTime) {
		this.bestTime = bestTime;
	}

	
}
