/**
 * 
 */
package za.ac.uj.csc3a10.Characters;

/**
 * @author Mazibila A.K.
 *
 */
public class AI extends GameCharacter 
{
	public AI() 
	{
		this.setName("CPU");
		this.setSpeed(0.1f);
		this.setxPos(0);
		this.setyPos(0);
	}

	public AI(String name, float speed, int xPos, int yPos) {
		super(name, speed, xPos, yPos);
	}
	
}
